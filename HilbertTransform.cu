/*
 * HilbertTransform.cu
 *
 *  Created on: Dec 27, 2014
 *      Author: dhyun
 *
 *  Note: numSamples is mostly ignored by HilbertTransform. It exists simply to pass through
 * 		  to gpuFocus.
 */

#include "HilbertTransform.cuh"

namespace gpuBF {

template <typename T_in, typename T_out>
HilbertTransform<T_in,T_out>::HilbertTransform() {
	initValuesToZero();
}
template <typename T_in, typename T_out>
void HilbertTransform<T_in,T_out>::initValuesToZero() {
	// Base class members
	this->devID = 0;
	this->stream = 0;
	this->isInit = false;
	this->verb = 0;
	memset(&(this->props),   0, sizeof(this->props));
	memset(&(this->outDesc), 0, sizeof(this->outDesc));
	this->d_out = NULL;

	// HilbertTransform members
	fftplan = 0;
	d_raw = NULL;
	d_fft = NULL;
	memset(&rawDesc, 0, sizeof(rawDesc));
	memset(&fftDesc, 0, sizeof(fftDesc));
}

template <typename T_in, typename T_out>
void HilbertTransform<T_in,T_out>::initialize(DataProcessor<T_in> *R,
                                              cudaStream_t cudaStream,
                                              int verbosity) {
	initialize(R->getOutputDevPtr(),
	           R->getOutputDesc(),
	           R->getDeviceID(),
	           cudaStream,
	           verbosity);
}

template <typename T_in, typename T_out>
void HilbertTransform<T_in,T_out>::initialize(T_in *d_complexRF,
                                              gpuArrayDesc inputDesc,
                                              int deviceID,
                                              cudaStream_t cudaStream,
                                              int verbosity) {
	// Base class members
	this->devID = deviceID;
	this->stream = cudaStream;
	this->verb = verbosity;
	CCE(cudaGetDeviceProperties(&(this->props), this->devID));
	if (this->verb > 0)
		printf("Initializing HilbertTransform object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	this->outDesc = inputDesc;

	// HilbertTransform members
	rawDesc = inputDesc;
	d_raw = d_complexRF;
	fftDesc = inputDesc;
	fftDesc.typeSize = sizeof(float2);

	// Initialize arrays
	initializeArrays();
	this->isInit = true;
}

// If the output is not float2, an intermediate float2 array must be initialized
template <typename T>
inline void initializeIntermediateArray(T       *d_out, gpuArrayDesc  outDesc,
                                        float2 **d_fft, gpuArrayDesc *fftDesc) {
	makePitchedArray((void **)d_fft, fftDesc);
}
// If the output is float2, simply use the output array as the FFT array
template <>
inline void initializeIntermediateArray(float2  *d_out, gpuArrayDesc  outDesc,
                                        float2 **d_fft, gpuArrayDesc *fftDesc) {
	*d_fft = d_out;
	*fftDesc = outDesc;
}

template <typename T_in, typename T_out>
void HilbertTransform<T_in,T_out>::initializeArrays() {
	CCE(cudaSetDevice(this->devID));
	// Initialize output array
	makePitchedArray((void **)&(this->d_out), &(this->outDesc));
	if (this->verb > 1) printDesc(this->devID, this->outDesc, "Complex RF data");
	CCE(cudaMemset(this->d_out, 0, getSizeInBytes(this->outDesc)));
	// Initialize intermediate array if output is of different type
	initializeIntermediateArray(this->d_out, this->outDesc, &d_fft, &fftDesc);
	// Also set up the FFTs that are needed to generate IQ data
	if (cufftCreate(&fftplan) != CUFFT_SUCCESS)
		fprintf(stderr,"Error creating FFT plan.\n");
	if (cufftPlanMany(&fftplan, 1, &fftDesc.size.w, NULL, 0, 0, NULL, 0, 0,CUFFT_C2C,
			fftDesc.size.y*fftDesc.size.z) != CUFFT_SUCCESS)
		fprintf(stderr,"Error initializing FFT plan.\n");
	cufftSetStream(fftplan, this->stream);
}

// If an intermediate array was formed, free it.
template <typename T>
inline void freeIntermediateArray(T *d_out, float2 *d_fft) { CCE(cudaFree(d_fft)); }
// Otherwise, do nothing.
template <>
inline void freeIntermediateArray(float2 *d_out, float2 *d_fft) {};

template <typename T_in, typename T_out>
void HilbertTransform<T_in,T_out>::reset() {
	if (this->verb > 0)
		printf("Clearing HilbertTransform object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	CCE(cudaSetDevice(this->devID));
	freeIntermediateArray(this->d_out, d_fft);
	CCE(cudaFree(this->d_out));
	if (fftplan != 0) cufftDestroy(fftplan);
	initValuesToZero();
	this->isInit = false;
}

template <typename T_in, typename T_out>
HilbertTransform<T_in,T_out>::~HilbertTransform() {
	reset();
}

// Convert to float2 if input is not float2.
template <typename T_in>
inline void convertToTypeFloat2IfNeeded(dim3 B, dim3 G, cudaStream_t stream,
                                        T_in *d_raw, gpuArrayDesc rawDesc,
                                        float2 *d_fft, gpuArrayDesc fftDesc) {
	convertToTypeFloat2<<<G,B,0,stream>>>(d_raw, rawDesc.size, d_fft, fftDesc.size.w);
}
// Do nothing if the input is already float2.
template <>
inline void convertToTypeFloat2IfNeeded(dim3 B, dim3 G, cudaStream_t stream,
                                        float2 *d_raw, gpuArrayDesc rawDesc,
                                        float2 *d_fft, gpuArrayDesc fftDesc) {};
// Convert to float2 if input is not float2.
template <typename T_out>
inline void convertFromTypeFloat2IfNeeded(dim3 B, dim3 G, cudaStream_t stream,
                                          float2 *d_fft, gpuArrayDesc fftDesc,
                                          T_out *d_out, gpuArrayDesc outDesc) {
	convertFromTypeFloat2<<<G,B,0,stream>>>(d_fft, fftDesc.size, d_out, outDesc.size.w);
}
// Do nothing if the input is already float2.
template <>
inline void convertFromTypeFloat2IfNeeded(dim3 B, dim3 G, cudaStream_t stream,
                                          float2 *d_fft, gpuArrayDesc fftDesc,
                                          float2 *d_out, gpuArrayDesc outDesc) {};


// Hilbert transform of raw RF data
template <typename T_in, typename T_out>
void HilbertTransform<T_in,T_out>::applyHilbertTransform() {
	dim3 B(256,1,1);
	dim3 G((this->outDesc.size.w-1)/B.x+1, this->outDesc.size.y, this->outDesc.size.z);
	// If the data type is not float2, copy data from input array
	convertToTypeFloat2IfNeeded(B, G, this->stream, d_raw, rawDesc, d_fft, fftDesc);
	// Apply forward FFT
	cufftExecC2C(fftplan, d_fft, d_fft, CUFFT_FORWARD);
	// Flip all negative frequencies to positive
	// (Pitch size should always be even, so call the even version)
	hilbertWeightKernelEven<<<G,B,0,this->stream>>>(d_fft, fftDesc.size);
	// Apply inverse FFT
	cufftExecC2C(fftplan, d_fft, d_fft, CUFFT_INVERSE);
	// If the data type is not float2, copy data to output array
	convertFromTypeFloat2IfNeeded(B, G, this->stream, d_fft, fftDesc, this->d_out, this->outDesc);
}

// Apply weights to first dimension according to MATLAB's Hilbert transform
// The applied weights are slightly different for an odd and even first dimension
// length.
__global__ void hilbertWeightKernelOdd(float2 *data, int4 size) {
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	int tidy = threadIdx.y + blockIdx.y*blockDim.y;
	int tidz = threadIdx.z + blockIdx.z*blockDim.z;
	int idx = tidx + size.w*(tidy + size.y*tidz);
	float weight;

	// Check bounds
	if (tidx < size.w && tidy < size.y && tidz < size.z) {
		if 		(tidx < 1) 				weight = 1.0f;
		else if (tidx < (size.w+1)/2)	weight = 2.0f;
		else							weight = 0.0f;
		data[idx] *= weight;
	}
}
__global__ void hilbertWeightKernelEven(float2 *data, int4 size) {
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	int tidy = threadIdx.y + blockIdx.y*blockDim.y;
	int tidz = threadIdx.z + blockIdx.z*blockDim.z;
	int idx = tidx + size.w*(tidy + size.y*tidz);
	float weight;

	// Check bounds
	if (tidx < size.w && tidy < size.y && tidz < size.z) {
		if 		(tidx < 1)			weight = 1.0f;
		else if (tidx < size.w/2) 	weight = 2.0f;
		else if (tidx < size.w/2+1) weight = 1.0f;
		else 						weight = 0.0f;
		data[idx] *= weight;
	}
}

template <typename T_in>
__global__ void convertToTypeFloat2(T_in *raw,
                                    int4 rawSize,
                                    float2 *fft,
                                    int fftSize_w) {
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	int tidy = threadIdx.y + blockIdx.y*blockDim.y;
	int tidz = threadIdx.z + blockIdx.z*blockDim.z;
	if (tidx < rawSize.x && tidy < rawSize.y && tidz < rawSize.z) {
		fft[tidx + fftSize_w*(tidy + rawSize.y*tidz)] = 
			loadData2(raw, rawSize.w*(tidy + rawSize.y*tidz));
	}
}
template <typename T_out>
__global__ void convertFromTypeFloat2(float2 *fft,
                                      int4 fftSize,
                                      T_out *out,
                                      int outSize_w) {
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	int tidy = threadIdx.y + blockIdx.y*blockDim.y;
	int tidz = threadIdx.z + blockIdx.z*blockDim.z;
	if (tidx < fftSize.x && tidy < fftSize.y && tidz < fftSize.z) {
		saveData2(out,
		          tidx + outSize_w*(tidy + fftSize.y*tidz),
		          fft[tidx + fftSize.w*(tidy + fftSize.y*tidz)]);
	}
}

// Real input
template class HilbertTransform<short, short2>;
template class HilbertTransform<int,   short2>;
template class HilbertTransform<half,  short2>;
template class HilbertTransform<float, short2>;
template class HilbertTransform<short, half2>;
template class HilbertTransform<int,   half2>;
template class HilbertTransform<half,  half2>;
template class HilbertTransform<float, half2>;
template class HilbertTransform<short, float2>;
template class HilbertTransform<int,   float2>;
template class HilbertTransform<half,  float2>;
template class HilbertTransform<float, float2>;
// Complex input
template class HilbertTransform<short2,short2>;
template class HilbertTransform<int2,  short2>;
template class HilbertTransform<half2, short2>;
template class HilbertTransform<float2,short2>;
template class HilbertTransform<short2,half2>;
template class HilbertTransform<int2,  half2>;
template class HilbertTransform<half2, half2>;
template class HilbertTransform<float2,half2>;
template class HilbertTransform<short2,float2>;
template class HilbertTransform<int2,  float2>;
template class HilbertTransform<half2, float2>;
template class HilbertTransform<float2,float2>;


/*
// If a real-valued type is provided, interleave zeros first.
template <typename T>
void HilbertTransform<T_in,T_out>::applyHilbertTransform(T *d_realRF) {
	// Convert real RF data to complex RF data by interleaving zeros
	dim3 B(256,1,1);
	dim3 G((hilbDesc.size.w-1)/B.x+1, hilbDesc.size.y, hilbDesc.size.z);
	interleaveZeros<<<G,B,0,stream>>>(d_realRF, hilbDesc.size, d_compRF);
	applyHilbertTransform(d_compRF);
}
// Template instantiation
template void HilbertTransform<T_in,T_out>::applyHilbertTransform(short*);	// 16-bit integer
template void HilbertTransform<T_in,T_out>::applyHilbertTransform(int*);		// 32-bit integer
template void HilbertTransform<T_in,T_out>::applyHilbertTransform(float*);	// 32-bit floating point
template void HilbertTransform<T_in,T_out>::applyHilbertTransform(double*);	// 64-bit floating point

// Kernels
template <typename T>
__global__ void interleaveZeros(T *real, int4 size, float2 *comp) {
	// Thread information
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	int tidy = threadIdx.y + blockIdx.y*blockDim.y;
	int tidz = threadIdx.z + blockIdx.z*blockDim.z;
	int idx = tidx + size.w*(tidy + size.y*tidz);
	// Check bounds
	if (tidx < size.w && tidy < size.y && tidz < size.z)
		comp[idx] = make_float2((float)real[idx], 0.0f);
}
*/

} // namespace gpuBF
