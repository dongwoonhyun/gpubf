/**	@file Focus.cuh
	@date Sep 16, 2014
	@author Dongwoon Hyun (dhyun)
*/

#ifndef FOCUS_CUH_
#define FOCUS_CUH_

#include "gpuBF.cuh"
#include "AlignNxFc.cuh"

namespace gpuBF {

/**	@brief Class to apply focusing delays to data.

	This class applies focusing delays and apodization profiles to the
	inputted data. The data must be complex, but can be either radiofrequency
	or baseband. The user provides delay and apodization profiles for the
	data. For focused transmit sequences, only receive delays (and
	apodizations) are provided. For synthetic aperture sequences (e.g.,
	diverging wave, plane wave), the transmit delays must be provided as well.

*/
template <typename T_in, typename T_out>
class Focus: public DataProcessor<T_out> {
private:

	// CUDA objects
	cudaTextureObject_t tex;
	cudaTextureDesc texDesc;
	cudaResourceDesc resDesc;

	// Raw (unfocused) data parameters
	gpuArrayDesc rawDesc;
	T_in *d_raw;
	float spcy; // Samples per cycle; only necessary if focusing baseband data

	// Delay and apodization tables
	gpuArrayDesc RxDesc, TxDesc;
	float *d_delayTx, *d_apodTx;
	float *d_delayRx, *d_apodRx;
	bool freeTable[4]; // Flag to keep track of which tables to free

	// Internal functions
	void initValuesToZero();
	void initTextureObject(T_in *d_ptr);
	void initializeArrays();
	void checkInit();

public:

	// Constructor and destructor
	Focus();
	virtual ~Focus();

	// Initialization steps
	/** @brief Function to initialize the object directly.
	@param d_unfocused Device pointer to IQ data (baseband or RF).
	@param unfocDesc Description of input data.
	@param deviceID GPU to use. Defaults to 0.
	@param cudaStream cudaStream to use. Defaults to 0.
	@param verbosity Level of verbosity. Defaults to 1.
 	*/
	void initialize(T_in *d_unfocused,
					gpuArrayDesc unfocDesc,
					int deviceID=0,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	/** @brief Alternate function to initialize the object using a generic DataProcessor object.
	@param R DataProcessor object whose output is IQ data (baseband or RF).
	@param cudaStream cudaStream to use. Defaults to 0.
	@param verbosity Level of verbosity. Defaults to 1.
 	*/
	void initialize(DataProcessor<T_in> *R,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	
	/** @brief Alternate function to initialize the object using an
		AlignNxFc object. This function also extracts the samplesPerCycle.
	@param A AlignNxFc object whose output is IQ data (baseband only).
	@param cudaStream cudaStream to use. Defaults to 0.
	@param verbosity Level of verbosity. Defaults to 1.
 	*/
	void initialize(AlignNxFc<short2,T_in> *A,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	void initialize(AlignNxFc<float2,T_in> *A,
					cudaStream_t cudaStream=0,
					int verbosity=1);

	/** @brief Function to load the relevant TX tables from the host to the device (synthetic aperture only).
	@param h_delayTx Host pointer to transmit delay table with dimensions (nrows\f$\times\f$nxmits\f$\times\f$ncols).
	@param h_apodTx Host pointer to transmit apodization table with dimensions (nrows\f$\times\f$nxmits\f$\times\f$ncols).
	@param nrows Number of reconstructed rows of pixels
	@param nxmits Number of compounded transmits
	@param ncols Number of reconstructed columns of pixels
 	*/
 	void loadTxTables(float *h_delayTx,
	                  float *h_apodTx,
	                  int nrows,
	                  int nxmits,
	                  int ncols);
	/** @brief Function to load the RX tables from the host to the device.
	@param h_delayRx Host pointer to receive delay table with dimensions (nrows\f$\times\f$nxmits\f$\times\f$ncols).
	@param h_apodRx Host pointer to receive apodization table with dimensions (nrows\f$\times\f$nxmits\f$\times\f$ncols).
	@param nrows Number of reconstructed rows of pixels
	@param nchans Number of receive channels
	@param ncols Number of reconstructed columns of pixels
 	*/
 	void loadRxTables(float *h_delayRx,
	                  float *h_apodRx,
	                  int nrows,
	                  int nchans,
	                  int ncols);

	/// @brief Set the output image sampling frequency for demodulation.
	void setOutputWlps(float output_wlps);

	/// @brief Change device pointer to unfocused data
	void setNewRawDataPtr(T_in *dev_newInputPtr);

	/// @brief Set the samples per cycle of the input to apply compensatory phase shifts when focusing baseband data
	void setSamplesPerCycle(float sampsPerCycle);

	/// @brief Free all dynamically allocated memory.
	void reset();
	
	// Apply focusing
	/// @brief Apply focusing and apodization to baseband data
	void focusBasebandData();
	/// @brief Apply focusing and apodization to modulated data
	void focusModulatedData();

};

/// @cond KERNELS
/**	@addtogroup FocusKernels Focus Kernels
	@{
*/

/** @brief Kernel to focus baseband data in a 1:1 Tx:Rx configuration.
@param tex Texture object of the raw data.
@param cyclesPerSample Number of cycles per sample (fc/fs) of <i>input</i> data.
@param delayRx Combined transmit and receive delay table.
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param delSize_w Pitch of the delay table (in elements).
@param focIQ Device pointer to the output focused data.
@param focSize Dimensions of focused data on the GPU.
*/
template <typename T_out>
 __global__ void focusBB1to1(cudaTextureObject_t tex,
                            float cyclesPerSample,
                            float *delayRx,
                            float outwlps,
                            int delSize_w,
                            T_out *focIQ,
                            int4 focSize,
                            float normFactor);
/** @brief Kernel to focus baseband data in a synthetic aperture configuration.
@param tex Texture object of the raw data.
@param cyclesPerSample Number of cycles per sample (fc/fs) of <i>input</i> data.
@param delayRx Receive delay table.
@param delayTx Transmit delay table.
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param delSize_w Pitch of the delay table (in elements).
@param nxmits Number of compounded transmit events.
@param focIQ Device pointer to the output focused data.
@param focSize Dimensions of focused data on the GPU.
*/
template <typename T_out>
__global__ void focusBBCompFull(cudaTextureObject_t tex,
								float cyclesPerSample,
								float *delayRx,
								float *delayTx,
								float outwlps,
								int delSize_w,
								int nxmits,
								T_out *focIQ,
								int4 focSize,
								float normFactor);
/** @brief Kernel to focus baseband data in a synthetic aperture configuration with transmit apodization.
@param tex Texture object of the raw data.
@param cyclesPerSample Number of cycles per sample (fc/fs) of <i>input</i> data.
@param delayRx Receive delay table.
@param delayTx Transmit delay table.
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param delSize_w Pitch of the delay table (in elements).
@param apodTx Apodization weights on transmit events.
@param nxmits Number of compounded transmit events.
@param focIQ Device pointer to the output focused data.
@param focSize Dimensions of focused data on the GPU.
*/
template <typename T_out>
__global__ void focusBBCompApod(cudaTextureObject_t tex,
								float cyclesPerSample,
								float *delayRx,
								float *delayTx,
								float outwlps,
								int delSize_w,
								float *apodTx,
								int nxmits,
								T_out *focIQ,
								int4 focSize,
								float normFactor);

/** @brief Kernel to focus modulated data in a 1:1 Tx:Rx configuration.
@param tex Texture object of the raw data.
@param delayRx Combined transmit and receive delay table.
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param delSize_w Pitch of the delay table (in elements).
@param focIQ Device pointer to the output focused data.
@param focSize Dimensions of focused data on the GPU.
*/
template <typename T_out>
__global__ void focusRF1to1(cudaTextureObject_t tex,
							float *delayRx,
							float outwlps,
							int delSize_w,
							T_out *focIQ,
							int4 focSize,
							float normFactor);
/** @brief Kernel to focus modulated data in a synthetic aperture configuration.
@param tex Texture object of the raw data.
@param delayRx Receive delay table.
@param delayTx Transmit delay table.
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param delSize_w Pitch of the delay table (in elements).
@param nxmits Number of compounded transmit events.
@param focIQ Device pointer to the output focused data.
@param focSize Dimensions of focused data on the GPU.
*/
template <typename T_out>
__global__ void focusRFCompFull(cudaTextureObject_t tex,
								float *delayRx,
								float *delayTx,
								float outwlps,
								int delSize_w,
								int nxmits,
								T_out *focIQ,
								int4 focSize,
								float normFactor);
/** @brief Kernel to focus modulated data in a synthetic aperture configuration with transmit apodization.
@param tex Texture object of the raw data.
@param delayRx Receive delay table.
@param delayTx Transmit delay table.
@param outwlps Wavelengths per output sample (2*fs/fc) of <i>output</i> data.
@param delSize_w Pitch of the delay table (in elements).
@param apodTx Apodization weights on transmit events.
@param nxmits Number of compounded transmit events.
@param focIQ Device pointer to the output focused data.
@param focSize Dimensions of focused data on the GPU.
*/
template <typename T_out>
__global__ void focusRFCompApod(cudaTextureObject_t tex,
								float *delayRx,
								float *delayTx,
								float outwlps,
								int delSize_w,
								float *apodTx,
								int nxmits,
								T_out *focIQ,
								int4 focSize,
								float normFactor);

/** @brief Kernel to apply receive apodization to focused channel data.
@param out Pre-apodized focused channel data.
@param apodRx Receive apodization table.
@param outSize Dimensions of focused data on the GPU.
@param apoSize_w Pitch of the apodization table (in elements).
*/
template <typename T_out>
__global__ void applyRxApod(T_out *out,
                            float *apodRx,
                            int4 outSize,
							int apoSize_w);

/** @}*/
/// @endcond

} // namespace gpuBF

#endif /* FOCUS_CUH_ */

