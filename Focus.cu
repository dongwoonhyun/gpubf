/*
 * Focus.cu
 *
 *  Created on: Sep 16, 2014
 *      Author: dhyun
 */

#include "Focus.cuh"

namespace gpuBF {

template <typename T_in, typename T_out>
Focus<T_in,T_out>::Focus() {
	//printf("Instantiating Focus object...\n");
	initValuesToZero();
}
template <typename T_in, typename T_out>
void Focus<T_in,T_out>::initValuesToZero() {
	// Base class members
	this->devID = 0;
	this->stream = 0;
	this->isInit = false;
	this->verb = 0;
	memset(&(this->props),   0, sizeof(this->props));
	memset(&(this->outDesc), 0, sizeof(this->outDesc));
	this->d_out = NULL;

	// Focus members
	memset(&texDesc, 0, sizeof(texDesc));
	memset(&resDesc, 0, sizeof(resDesc));
	memset(&rawDesc, 0, sizeof(rawDesc));
	d_raw = NULL;
	spcy = 0.f;
	memset(&TxDesc, 0, sizeof(TxDesc));
	memset(&RxDesc, 0, sizeof(RxDesc));
	d_delayTx = NULL;
	d_apodTx  = NULL;
	d_delayRx = NULL;
	d_apodRx  = NULL;
	for (int i = 0; i < 4; i++) { freeTable[i] = false; }
}

template <typename T_in, typename T_out>
void Focus<T_in,T_out>::reset() {
	if (this->verb > 0)
		printf("Clearing Focus object on GPU #%d (%s).\n", 
		       this->devID, this->props.name);
	CCE(cudaSetDevice(this->devID));
	if (freeTable[0]) {	CCE(cudaFree(d_delayTx)); }
	if (freeTable[1]) { CCE(cudaFree(d_apodTx));  }
	if (freeTable[2]) { CCE(cudaFree(d_delayRx)); }
	if (freeTable[3]) { CCE(cudaFree(d_apodRx));  }
	CCE(cudaFree(this->d_out));
	CCE(cudaDestroyTextureObject(tex));
	initValuesToZero();
}
template <typename T_in, typename T_out>
Focus<T_in,T_out>::~Focus() {
	reset();
}

// Public functions
template <typename T_in, typename T_out>
void Focus<T_in,T_out>::initialize(T_in *d_unfocused, gpuArrayDesc unfocDesc, int deviceID, cudaStream_t cudaStream, int verbosity) {
	// Base class members
	this->devID = deviceID;
	this->stream = cudaStream;
	this->verb = verbosity;
	CCE(cudaGetDeviceProperties(&(this->props), this->devID));
	if (this->verb > 0)
		printf("Initializing Focus object on GPU #%d (%s).\n",
		       this->devID, this->props.name);

	// Focus members
	rawDesc = unfocDesc;
	validateDesc(rawDesc, __FILE__, __LINE__);
	d_raw = d_unfocused;
	initTextureObject(d_raw);
}
template <typename T_in, typename T_out>
void Focus<T_in,T_out>::initialize(DataProcessor<T_in> *R, cudaStream_t cudaStream, int verbosity) {
	initialize(R->getOutputDevPtr(),
	           R->getOutputDesc(),
	           R->getDeviceID(),
	           cudaStream,
	           verbosity);
}
template <typename T_in, typename T_out>
void Focus<T_in,T_out>::initialize(AlignNxFc<short2,T_in> *A, cudaStream_t cudaStream, int verbosity) {
	initialize(A->getOutputDevPtr(),
	           A->getOutputDesc(),
	           A->getDeviceID(),
	           cudaStream,
	           verbosity);
	setSamplesPerCycle(A->getSamplesPerCycle());
}
template <typename T_in, typename T_out>
void Focus<T_in,T_out>::initialize(AlignNxFc<float2,T_in> *A, cudaStream_t cudaStream, int verbosity) {
	initialize(A->getOutputDevPtr(),
	           A->getOutputDesc(),
	           A->getDeviceID(),
	           cudaStream,
	           verbosity);
	setSamplesPerCycle(A->getSamplesPerCycle());
}

inline void setTextureObjectModes(float2 *d_ptr, cudaTextureDesc &texDesc) {
	texDesc.readMode = cudaReadModeElementType;
}
inline void setTextureObjectModes(short2 *d_ptr, cudaTextureDesc &texDesc) {
	texDesc.readMode = cudaReadModeNormalizedFloat;
}
inline void setTextureObjectModes(half2  *d_ptr, cudaTextureDesc &texDesc) {
	texDesc.readMode = cudaReadModeNormalizedFloat;
}

template <typename T_in, typename T_out>
void Focus<T_in,T_out>::initTextureObject(T_in *d_ptr) {
	// Set up textures to use the built-in bilinear interpolation hardware
	// Use texture objects (CC >= 3.0)
	// Texture description
	texDesc.addressMode[0]	 = cudaAddressModeBorder;
	texDesc.addressMode[1]	 = cudaAddressModeBorder;
	texDesc.filterMode 		 = cudaFilterModeLinear;
	texDesc.normalizedCoords = 0;
	setTextureObjectModes(d_ptr, texDesc);
	// Resource description
	resDesc.resType 		   = cudaResourceTypePitch2D;
	resDesc.res.pitch2D.width  = rawDesc.size.x;
	resDesc.res.pitch2D.height = rawDesc.size.y*rawDesc.size.z;
	resDesc.res.pitch2D.desc   = cudaCreateChannelDesc<T_in>();
	resDesc.res.pitch2D.devPtr = d_ptr;
	resDesc.res.pitch2D.pitchInBytes = rawDesc.pitchInBytes;
	// Create texture object
	CCE(cudaSetDevice(this->devID));
	CCE(cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL));
}
template <typename T_in, typename T_out>
void Focus<T_in,T_out>::initializeArrays() {
	CCE(cudaSetDevice(this->devID));
	CCE(cudaFree(this->d_out));
	makePitchedArray((void **)&(this->d_out), &(this->outDesc));
	if (this->verb > 1) printDesc(this->devID, this->outDesc, "Focused data");
}
template <typename T_in, typename T_out>
void Focus<T_in,T_out>::loadTxTables(float *h_delayTx, float *h_apodTx, int nrows, int nxmits, int ncols) {
	// Set CUDA device
	CCE(cudaSetDevice(this->devID));
	if (h_delayTx != NULL) {
		TxDesc.size.x = nrows;
		TxDesc.size.y = nxmits;
		TxDesc.size.z = ncols;
		TxDesc.typeSize = sizeof(float);
		if (freeTable[0]) {	CCE(cudaFree(d_delayTx)); }
		makePitchedArray((void **)&d_delayTx, &TxDesc);
		freeTable[0] = true;
		if (this->verb > 1) printDesc(this->devID, TxDesc, "Tx delay table");
		CCE(cudaMemcpy2D(d_delayTx,
		    TxDesc.pitchInBytes,
		    h_delayTx,
		    sizeof(float)*TxDesc.size.x,
		    sizeof(float)*TxDesc.size.x,
		    TxDesc.size.y*TxDesc.size.z, HtoD));
		if (h_apodTx != NULL) {
			if (freeTable[1]) {	CCE(cudaFree(d_apodTx)); }
			makePitchedArray((void **)&d_apodTx, &TxDesc);
			freeTable[1] = true;
			if (this->verb > 1) printDesc(this->devID, TxDesc, "Tx apod table");
			CCE(cudaMemcpy2D(d_apodTx,
			    TxDesc.pitchInBytes,
			    h_apodTx,
			    sizeof(float)*TxDesc.size.x,
			    sizeof(float)*TxDesc.size.x,
			    TxDesc.size.y*TxDesc.size.z, HtoD));
		}
	}
}
template <typename T_in, typename T_out>
void Focus<T_in,T_out>::loadRxTables(float *h_delayRx, float *h_apodRx, int nrows, int nchans, int ncols) {
	RxDesc.size.x = nrows;
	RxDesc.size.y = nchans;
	RxDesc.size.z = ncols;
	RxDesc.typeSize = sizeof(float);
	// Set CUDA device
	CCE(cudaSetDevice(this->devID));
	if (freeTable[2]) {	CCE(cudaFree(d_delayRx)); }
	makePitchedArray((void **)&d_delayRx, &RxDesc);
	freeTable[2] = true;
	if (this->verb > 1) printDesc(this->devID, RxDesc, "Rx delay table");
	CCE(cudaMemcpy2D(d_delayRx,
					 RxDesc.pitchInBytes,
				     h_delayRx,
				     sizeof(float)*RxDesc.size.x,
				     sizeof(float)*RxDesc.size.x,
				     RxDesc.size.y*RxDesc.size.z, HtoD));
	if (h_apodRx != NULL) {
		if (freeTable[3]) {	CCE(cudaFree(d_apodRx)); }
		makePitchedArray((void **)&d_apodRx, &RxDesc);
		freeTable[3] = true;
		if (this->verb > 1) printDesc(this->devID, TxDesc, "Rx apod table");
		CCE(cudaMemcpy2D(d_apodRx,
		    			 RxDesc.pitchInBytes,
		    			 h_apodRx,
		    			 sizeof(float)*RxDesc.size.x,
		    			 sizeof(float)*RxDesc.size.x,
		    			 RxDesc.size.y*RxDesc.size.z, HtoD));
	}
	// Also set up output
	this->outDesc = RxDesc;
	this->outDesc.typeSize = sizeof(T_out);
	initializeArrays();
}

template <typename T_in, typename T_out>
void Focus<T_in,T_out>::setOutputWlps(float output_wlps) {
	this->outDesc.wlps = output_wlps;
	if (this->verb > 1)
		printf("Set output wlps to %f for Focus object on GPU #%d (%s).\n",
		       this->outDesc.wlps, this->devID, this->props.name);
}
template <typename T_in, typename T_out>
void Focus<T_in,T_out>::setSamplesPerCycle(float samplesPerCycle) {
	spcy = samplesPerCycle;
	if (this->verb > 1)
		printf("Set input samples per cycle to %f for Focus object on GPU #%d (%s).\n",
		       spcy, this->devID, this->props.name);
}

template <typename T_in, typename T_out>
void Focus<T_in,T_out>::setNewRawDataPtr(T_in *dev_newInputPtr) {
	CCE(cudaSetDevice(this->devID));
	CCE(cudaDestroyTextureObject(tex));
	initTextureObject(dev_newInputPtr);
}

template <typename T_in, typename T_out>
void Focus<T_in,T_out>::checkInit() {
	// Confirm that things are initialized properly
	if (!this->isInit) {
		if (d_delayTx == NULL) {
			if (rawDesc.size.y != this->outDesc.size.y ||
			    rawDesc.size.z != this->outDesc.size.z) {
				fprintf(stderr, "%s(%i) : %s%s\n",
				        __FILE__, __LINE__,
				        "Transmit delays were not provided, but input and ",
				        "output descriptions do not match.\n");
				printDesc(this->devID, rawDesc, "Unfocused data");
				printDesc(this->devID, this->outDesc, "Focused data");
			}
		}
		else {
			if (TxDesc.size.x != this->outDesc.size.x ||
			    TxDesc.size.y !=       rawDesc.size.z ||
			    TxDesc.size.z != this->outDesc.size.z) {
			    fprintf(stderr, "%s(%i) : %s%s\n",
			    		__FILE__, __LINE__,
			    		"Transmit delays were provided, but the description ",
			    		"does not match the raw data.\n");
				printDesc(this->devID, TxDesc,  "Tx delay table");
				printDesc(this->devID, rawDesc, "Unfocused data");
				printDesc(this->devID, this->outDesc, "Focused data");
			}
		}
		this->isInit = true;
		if (this->verb > 1)
			printf("GPU [%d]: Focus object is properly initialized.\n", this->devID);
	}
}

template <typename Ti, typename To>
inline float getNormalizationFactor(Ti *d_in, To *d_out) { return 32767.f; }
template<>
inline float getNormalizationFactor(float2 *d_in, float2 *d_out) { return 1.f; }

template <typename T_in, typename T_out>
void Focus<T_in,T_out>::focusBasebandData() {
	checkInit();
	if (spcy == 0.f)
		fprintf(stderr, "%s(%i) : %s%s\n%s\n",
			    __FILE__, __LINE__,
			    "Attempted to focus baseband data without specifying the number ",
			    "of samples per cycle in the input.",
			    "Use Focus<T_in,T_out>::setSamplesPerCycle() to set this value.");

	// Set CUDA device
	CCE(cudaSetDevice(this->devID));
//	CCE(cudaMemsetAsync(this->d_out, 0, getSizeInBytes(this->outDesc), this->stream));

	// Get normalization factor
	float normFactor = getNormalizationFactor(d_raw, this->d_out);

	// Set up focusing block/grid sizes
	dim3 B(256,1,1);
	dim3 G((this->outDesc.size.w-1)/B.x+1,
	       (this->outDesc.size.y-1)/B.y+1,
	       (this->outDesc.size.z-1)/B.z+1);

	// If no Tx delays are provided, assume a one to one Tx/Rx
	if (d_delayTx == NULL) {
		focusBB1to1<<<G,B,0,this->stream>>>(tex, 1.0f/spcy, d_delayRx, this->outDesc.wlps,
		                                    RxDesc.size.w, this->d_out, this->outDesc.size,
		                                    normFactor);
	}
	// Otherwise, apply coherent compounding
	else {
		// If no Tx apodization is given, compound all transmits
		if (d_apodTx == NULL)
			focusBBCompFull<<<G,B,0,this->stream>>>(tex, 1.0f/spcy, d_delayRx, d_delayTx,
			                                        this->outDesc.wlps, RxDesc.size.w,
			                                        rawDesc.size.z, this->d_out, this->outDesc.size,
			                                        normFactor);
		// Otherwise, apply Tx apodization
		else
			focusBBCompApod<<<G,B,0,this->stream>>>(tex, 1.0f/spcy, d_delayRx, d_delayTx,
			                                        this->outDesc.wlps, RxDesc.size.w, d_apodTx,
			                                        rawDesc.size.z, this->d_out, this->outDesc.size,
			                                        normFactor);
	}

	// If requested, apply receive apodization
	if (d_apodRx != NULL) {
		applyRxApod<<<G,B,0,this->stream>>>(this->d_out, d_apodRx, this->outDesc.size, RxDesc.size.w);
	}
	getLastCudaError("Focusing kernel failed.\n");
}

template <typename T_in, typename T_out>
void Focus<T_in,T_out>::focusModulatedData() {
	checkInit();

	// Set CUDA device
	CCE(cudaSetDevice(this->devID));
//	CCE(cudaMemsetAsync(this->d_out, 0, getSizeInBytes(this->outDesc), this->stream));

	// Get normalization factor
	float normFactor = getNormalizationFactor(d_raw, this->d_out);

	// Set up focusing block/grid sizes
	dim3 B(256,1,1);
	dim3 G((this->outDesc.size.w-1)/B.x+1,
	       (this->outDesc.size.y-1)/B.y+1,
	       (this->outDesc.size.z-1)/B.z+1);

	// If no Tx delays are provided, assume a one to one Tx/Rx
	if (d_delayTx == NULL) {
		focusRF1to1<<<G,B,0,this->stream>>>(tex, d_delayRx, this->outDesc.wlps,
		                                    RxDesc.size.w, this->d_out, this->outDesc.size,
		                                    normFactor);
	}
	// Otherwise, apply coherent compounding
	else {
		// If no Tx apodization is given, compound all transmits
		if (d_apodTx == NULL)
			focusRFCompFull<<<G,B,0,this->stream>>>(tex, d_delayRx, d_delayTx,
			                                        this->outDesc.wlps, RxDesc.size.w,
			                                        rawDesc.size.z, this->d_out, this->outDesc.size,
			                                        normFactor);
		// Otherwise, apply Tx apodization
		else
			focusRFCompApod<<<G,B,0,this->stream>>>(tex, d_delayRx, d_delayTx,
			                                        this->outDesc.wlps, RxDesc.size.w, d_apodTx,
			                                        rawDesc.size.z, this->d_out, this->outDesc.size,
			                                        normFactor);
	}

	// If requested, apply receive apodization
	if (d_apodRx != NULL) {
		applyRxApod<<<G,B,0,this->stream>>>(this->d_out, d_apodRx, this->outDesc.size, RxDesc.size.w);
	}
	getLastCudaError("Focusing kernel failed.\n");
}

///////////////////////////////////////////////////////////////////////////
// Kernels
///////////////////////////////////////////////////////////////////////////
// Symmetric transmit and receive lines (i.e. receive along transmit lines)
// nsamps == nrows, etc.
template <typename T_out>
__global__ void focusBB1to1(cudaTextureObject_t tex,
                            float cyclesPerSample,
                            float *delayRx,
                            float outwlps,
                            int delSize_w,
                            T_out *focIQ,
                            int4 focSize,
                            float normFactor) {
	// Determine information about the current thread
	int row  = blockIdx.x*blockDim.x + threadIdx.x; // pixel row
	int chan = blockIdx.y*blockDim.y + threadIdx.y; // channel
	int col  = blockIdx.z*blockDim.z + threadIdx.z; // pixel column
	// Index of the input (delay) and output (focused data)
	int didx = row + delSize_w*(chan + focSize.y*col);
	int fidx = row + focSize.w*(chan + focSize.y*col);
	// Boundary check
	if (row < focSize.x && chan < focSize.y && col < focSize.z) {
		// Grab delay (in units of # of Tx samples)
		float delR = delayRx[didx];
		// Get the applied phase misalignment (in radians)
		float phase = -2.0f*PI*(2.0f*row*outwlps - delR*cyclesPerSample);
		// Get data and phase alignment factor
		float2 shift = make_float2(cosf(phase), sinf(phase));
		float2 data = tex2D<float2>(tex,delR+0.5f,chan+focSize.y*col+0.5f)*normFactor;
		// Store demodulated data
		float2 f;
		f.x = data.x*shift.x-data.y*shift.y;
		f.y = data.y*shift.x+data.x*shift.y;
		saveData2(focIQ, fidx, f);
	}
}
template <typename T_out>
__global__ void focusBBCompFull(cudaTextureObject_t tex,
								float cyclesPerSample,
								float *delayRx,
								float *delayTx,
								float outwlps,
								int delSize_w,
								int nxmits,
								T_out *focIQ,
								int4 focSize,
								float normFactor) {
	// Determine information about the current thread
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;
	// Index of the input (delay) and output (focused data)
	int didx = row + delSize_w*(chan + focSize.y*col);
	int fidx = row + focSize.w*(chan + focSize.y*col);

	// Boundary check
	if (row < focSize.x && chan < focSize.y && col < focSize.z) {
		// Get receive delay
		float delR = delayRx[didx];
		// Advance transmit delay pointer to current row and column
		delayTx += row + delSize_w*nxmits*col;
		// Initialize running sum
		float2 sum = make_float2(0.0f, 0.0f);
		float phase = -4.0f*PI*row*outwlps;
		// Compound all beams
		for (int tx = 0; tx < nxmits; tx++) {
			// Obtain round-trip delay
			float delRT = delR + delayTx[delSize_w*tx];
			// Get the applied phase misalignment (in radians)
			float tmp = phase + 2.0f*PI*delRT*cyclesPerSample;
			// Get data and phase alignment factor
			float2 shift = make_float2(cosf(tmp), sinf(tmp));
			// Read data
			float2 data = tex2D<float2>(tex,delRT+0.5f,chan+focSize.y*tx+0.5f)*normFactor;
			sum += make_float2(data.x*shift.x-data.y*shift.y,
							   data.y*shift.x+data.x*shift.y);
		}
		float2 f;
		f.x = sum.x; f.y = sum.y;
		saveData2(focIQ, fidx, f);
	}
}
template <typename T_out>
__global__ void focusBBCompApod(cudaTextureObject_t tex,
								float cyclesPerSample,
								float *delayRx,
								float *delayTx,
								float outwlps,
								int delSize_w,
								float *apodTx,
								int nxmits,
								T_out *focIQ,
								int4 focSize,
								float normFactor) {

	// Determine information about the current thread
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;
	// Index of the input (delay) and output (focused data)
	int didx = row + delSize_w*(chan + focSize.y*col);
	int fidx = row + focSize.w*(chan + focSize.y*col);

	// Boundary check
	if (row < focSize.x && chan < focSize.y && col < focSize.z) {
		// Get receive delay
		float delR = delayRx[didx];
		// Advance transmit delay pointer to current row and column
		delayTx += row + delSize_w*nxmits*col;
		apodTx  += row + delSize_w*nxmits*col;
		// Initialize running sum
		float2 sum = make_float2(0.0f, 0.0f);
		float phase = -4.0f*PI*row*outwlps;
		// Compound all beams
		for (int tx = 0; tx < nxmits; tx++) {
			// Obtain round-trip delay
			float delRT = delR + delayTx[delSize_w*tx];
			// Obtain the apodization for this transmit
			float apodT = apodTx[delSize_w*tx];
			// Get the applied phase misalignment (in radians)
			float tmp = phase + 2.0f*PI*delRT*cyclesPerSample;
			// Get data and phase alignment factor
			float2 shift = make_float2(cosf(tmp), sinf(tmp));
			// Read data
			float2 data = tex2D<float2>(tex,delRT+0.5f,chan+focSize.y*tx+0.5f)*normFactor;
			sum += make_float2(apodT*(data.x*shift.x-data.y*shift.y),
							   apodT*(data.y*shift.x+data.x*shift.y));
		}
		float2 f;
		f.x = sum.x; f.y = sum.y;
		saveData2(focIQ, fidx, f);
	}
}

template <typename T_out>
__global__ void focusRF1to1(cudaTextureObject_t tex,
							float *delayRx,
							float outwlps,
							int delSize_w,
							T_out *focIQ,
							int4 focSize,
                            float normFactor) {
	// Determine information about the current thread
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;
	// Index of the input (delay) and output (focused data)
	int didx = row + delSize_w*(chan + focSize.y*col);
	int fidx = row + focSize.w*(chan + focSize.y*col);
	// Boundary check
	if (row < focSize.x && chan < focSize.y && col < focSize.z) {
		// Grab delay
		float delR = delayRx[didx];
		// Get data and demodulation factor
		float phase = -4.0f*PI*row*outwlps;
		float2 demod = make_float2(cosf(phase), sinf(phase));
		float2 data = tex2D<float2>(tex,delR+0.5f,chan+focSize.y*col+0.5f)*normFactor;
		// Store demodulated data
		float2 f;
		f.x = data.x*demod.x-data.y*demod.y;
		f.y = data.y*demod.x+data.x*demod.y;
		saveData2(focIQ, fidx, f);
	}
}

template <typename T_out>
__global__ void focusRFCompFull(cudaTextureObject_t tex,
								float *delayRx,
								float *delayTx,
								float outwlps,
								int delSize_w,
								int nxmits,
								T_out *focIQ,
								int4 focSize,
                                float normFactor) {
	// Determine information about the current thread
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;
	// Index of the input (delay) and output (focused data)
	int didx = row + delSize_w*(chan + focSize.y*col);
	int fidx = row + focSize.w*(chan + focSize.y*col);

	// Boundary check
	if (row < focSize.x && chan < focSize.y && col < focSize.z) {
		// Get receive delay
		float delR = delayRx[didx];
		// Advance transmit delay pointer to current row and column
		delayTx += row + delSize_w*nxmits*col;
		// Initialize running sum
		float2 sum = make_float2(0.0f, 0.0f);
		float phase = -4.0f*PI*row*outwlps;
		float2 demod = make_float2(cos(phase), sin(phase));
		// Compound all beams
		for (int tx = 0; tx < nxmits; tx++) {
			// Obtain round-trip delay
			float delRT = delR + delayTx[delSize_w*tx];
			// Read data
			float2 data = tex2D<float2>(tex,delRT+0.5f,chan+focSize.y*tx+0.5f)*normFactor;
			sum.x += data.x;
			sum.y += data.y;
		}
		float2 f;
		f.x = sum.x*demod.x-sum.y*demod.y;
		f.y = sum.y*demod.x+sum.x*demod.y;
		saveData2(focIQ, fidx, f);
	}
}
template <typename T_out>
__global__ void focusRFCompApod(cudaTextureObject_t tex,
                                float *delayRx,
                                float *delayTx,
                                float outwlps,
                                int delSize_w,
                                float *apodTx,
                                int nxmits,
                                T_out *focIQ,
                                int4 focSize,
                                float normFactor) {
	// Determine information about the current thread
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;
	// Index of the input (delay) and output (focused data)
	int didx = row + delSize_w*(chan + focSize.y*col);
	int fidx = row + focSize.w*(chan + focSize.y*col);

	// Boundary check
	if (row < focSize.x && chan < focSize.y && col < focSize.z) {
		// Get receive delay
		float delR = delayRx[didx];
		// Advance transmit delay pointer to current row and column
		delayTx += row + delSize_w*nxmits*col;
		apodTx  += row + delSize_w*nxmits*col;
		// Initialize running sum
		float2 sum = make_float2(0.0f, 0.0f);
		float phase = -4.0f*PI*row*outwlps;
		float2 demod = make_float2(cos(phase), sin(phase));
		// Compound all beams
		for (int tx = 0; tx < nxmits; tx++) {
			// Obtain round-trip delay
			float delRT = delR + delayTx[delSize_w*tx];
			// Obtain the apodization for this transmit
			float apodT = apodTx[delSize_w*tx];
			// Read data
			float2 data = tex2D<float2>(tex,delRT+0.5f,chan+focSize.y*tx+0.5f)*normFactor;
			sum.x += apodT*data.x;
			sum.y += apodT*data.y;
		}
		float2 f;
		f.x = sum.x*demod.x-sum.y*demod.y;
		f.y = sum.y*demod.x+sum.x*demod.y;
		saveData2(focIQ, fidx, f);
	}
}

template <typename T_out>
__global__ void applyRxApod(T_out *foc,
                            float *apodRx,
                            int4 focSize,
                            int apoSize_w) {
	// Determine information about the current thread
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;
	int aidx = row + apoSize_w*(chan + focSize.y*col);
	int fidx = row + focSize.w*(chan + focSize.y*col);
	if (row < focSize.x && chan < focSize.y && col < focSize.z) {
		float2 tmp = loadData2(foc, fidx);
		saveData2(foc, fidx, tmp*apodRx[aidx]);
	}
}

template class Focus<float2,float2>;
template class Focus<short2,float2>;
template class Focus<float2,short2>;
template class Focus<short2,short2>;
template class Focus<short2,half2>;

} // namespace std


