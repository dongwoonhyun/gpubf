/** @file AlignNxFc.cuh
 *	@brief AlignNxFc class and member function prototypes
 *  @author Dongwoon Hyun (dhyun)
 *  @date Apr 8, 2015
 * 
 *  If the sampling frequency is exactly {1,2,4} times the center frequency,
 *  the received signal can actually be viewed instead as un-aligned and
 *  interleaved baseband signals. This code takes advantage of this to
 *  avoid needing to compute the Hilbert Transform in full.
 *
 *  NOTE: If samplesPerWave == 4, this code will modify the input data array
 *        to negate every other sample pair. The input MUST be writable.
 */
#ifndef ALIGNNXFC_CUH_
#define ALIGNNXFC_CUH_

#include "gpuBF.cuh"

namespace gpuBF {

/** @brief Class to properly align misaligned IQ data from the Verasonics

	The AlignNxFc class is a Verasonics-specific class that is used to take
	advantage of the Receive.samplesPerWave={4,2,1} parameter. The Verasonics
	directly samples the quadrature components with 200\%, 100\%, or 50\%
	bandwidth by using a sampling frequency of \f$4\f$\f$\times\f$\f$f_c\f$,
	\f$2\f$\f$\times\f$\f$f_c\f$, or \f$1\f$\f$\times\f$\f$f_c\f$,
	respectively. Technically, the received data is always sampled at
	\f$4\f$\f$\times\f$\f$f_c\f$, and only the samples [1-8], (\f$4\times
	f_c\f$), \[1, 2, 5, 6\] (\f$2\times f_c\f$), or \[1, 2\] (\f$1\times
	f_c\f$) are kept for every group of 8 samples.

 */
template <typename T_in, typename T_out>
class AlignNxFc: public DataProcessor<T_out> {
private:

	// CUDA objects
	cudaTextureObject_t tex; ///<Handle to the CUDA texture object that will be used to access the data
	cudaTextureDesc texDesc; ///<A description of the texture object properties (e.g., *.addressMode, *.filterMode)
	cudaResourceDesc resDesc; ///<A description of the texture object resources, such as data channel type and size

	// Input data
	T_in *d_raw; ///<Pointer to the raw data. The data in *d_raw does not belong to the AlignNxFc object
	gpuArrayDesc rawDesc;///<The gpuArrayDesc description of the input data
	int spcy;///<The number of samples per cycle (one-way propagation). Only values of 1, 2, and 4 are supported

	void initValuesToZero();
	void initializeArrays();
	/// @brief Initialize texture object. A texture is used to utilize
	/// the texture pipeline for fast data access.
	/// @param d_ptr Pointer to complex misaligned data
	void initTextureObject(T_in *d_ptr);

public:
	// Constructor
	AlignNxFc();
	// Destructor
	virtual ~AlignNxFc();

	/** @brief Function to initialize the AlignNxFc object directly.
	@param d_complexRF Device pointer to complex misaligned RF data.
	@param inputDesc Description of input data.
	@param samplesPerCycle Samples per cycle (1, 2, or 4).
	@param deviceID GPU to use. Defaults to 0.
	@param cudaStream cudaStream to use. Defaults to 0.
	@param verbosity Level of verbosity. Defaults to 1.
 	*/
	void initialize(T_in *d_complexRF,
					gpuArrayDesc inputDesc,
					int samplesPerCycle,
					int deviceID=0,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	/** @brief Alternate function to initialize the AlignNxFc using a generic DataProcessor object.
	@param R DataProcessor whose output is misaligned complex data.
	@param samplesPerCycle Samples per cycle (1, 2, or 4).
	@param cudaStream cudaStream to use. Defaults to 0.
	@param verbosity Level of verbosity. Defaults to 1.
 	*/
	void initialize(DataProcessor<T_in> *R,
					int samplesPerCycle,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	///@brief Execute CUDA kernels to properly align data.
	void alignBasebandData();

	///@brief Free dynamically allocated memory
	void reset();

	///@return Samples per cycle
	int getSamplesPerCycle() { return spcy; }
};

/// @cond KERNELS
/**	@addtogroup AlignNxFcKernels AlignNxFc Kernels
	@{
*/
/** @brief Negate every other pair of samples when spcy==4.

	When spcy==4, every other quadrature pair must be negated, i.e.,
	(+I,+Q,+I,+Q,...) -> (+I,+Q,-I,-Q,...), prior to alignment. This kernel
	does so and stores the result in *d_tmp. It is convenient to use *d_tmp =
	*d_raw, as long as it is safe to overwrite the data in *d_raw.

	@param orig Device pointer to complex non-negated misaligned data.
	@param size Size description of data (in elements).
	@param tmp Device pointer to complex negated misaligned data.
 */
template <typename T_in>
__global__ void negateEveryOtherKernel(T_in *orig, int4 size, T_in *tmp);
/** @brief Align quadrature components by interpolation

	(I1, Q2, I3, Q4, ...) -> (I1, Q1, I3, Q3, ...)
	@tparam spw Template parameter indicating samplesPerWave from Verasonics.
	@param tex Texture object to interpolate via hardware filtering.
	@param size Size description of data (in elements).
	@param data Device pointer to complex aligned data.
 */
template <typename T_out, int samplesPerWave>
__global__ void alignQuadratureKernel(cudaTextureObject_t tex, int4 size, T_out *data);
/// @endcond

}

#endif /* ALIGNNXFC_CUH_ */
