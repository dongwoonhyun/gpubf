/**	@file Focus.cuh
	@date Dec 30, 2014
	@author Dongwoon Hyun (dhyun)
*/

#ifndef BMODE_CUH_
#define BMODE_CUH_

#include "gpuBF.cuh"

namespace gpuBF {

/**	@brief Class to generate B-mode images

	This class sums channel data and can detect/compress the sum.

*/
template <typename T_in, typename T_out>
class Bmode: public DataProcessor<T_out> {
private:

	// Input array
	T_in *d_foc;
	gpuArrayDesc focDesc;

	// Functions
	void initValuesToZero();
	void initializeArrays();

public:
	Bmode();
	virtual ~Bmode();

	// Initialization steps
	void initialize(DataProcessor<T_in> *F,
	                cudaStream_t cudaStream=0,
	                int verbosity=1);
	void initialize(T_in *d_focused,
	                gpuArrayDesc desc,
	                int deviceID,
	                cudaStream_t cudaStream=0,
	                int verbosity=1);
	void reset();

	// Function to make B-mode image
	void makeBmode(bool applyCompression);
	// Function to set a new input data device pointer
	void setInputDevPtr(T_in *d_input);

};

template <typename T_in, typename T_out>
__global__ void sumDetectChannelData(T_in *foc,
                                     int4 focSize,
                                     T_out *out,
                                     int outSize_w);
template <typename T_in, typename T_out>
__global__ void sumDetectCompressChannelData(T_in *foc,
                                             int4 focSize,
                                             T_out *out,
                                             int outSize_w);
template <typename T_out>
__global__ void sumDetectChannelData_half2(half2 *foc,
                                           int4 focSize,
                                           T_out *out,
                                           int outSize_w);
template <typename T_out>
__global__ void sumDetectCompressChannelData_half2(half2 *foc,
                                                   int4 focSize,
                                                   T_out *out,
                                                   int outSize_w);
}

#endif /* BMODE_CUH_ */
