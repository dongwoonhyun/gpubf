SOURCES = Bmode.cu SLSC.cu Focus.cu AlignNxFc.cu HilbertTransform.cu PowerEstimator.cu VelocityEstimator.cu EnsembleFilter.cu
OBJECTS = $(SOURCES:.cu=.o)
CUFLAGS = -arch=sm_50 -O3 --use_fast_math -Xcompiler -fPIC -lineinfo
CUDADIR = /usr/local/cuda
INCLUDE = -I. -I$(CUDADIR)/samples/common/inc/
NVCC = $(CUDADIR)/bin/nvcc

all: $(SOURCES) libgpuBF.a testFunctions

libgpuBF.a: $(OBJECTS)
	ar rcs $@ $^

%.o: %.cu %.cuh gpuBF.cuh
	$(NVCC) $(INCLUDE) $(CUFLAGS) -dc $< -o $@

testFunctions:
	$(MAKE) -C test

clean:
	rm -f $(OBJECTS)
	
	
