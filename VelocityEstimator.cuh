/*
 * VelocityEstimator.cuh
 *
 *  Created on: Nov 18, 2015
 *      Author: dhyun
 *
 *  This class applies both conventional and channel-based velocity estimation.
 */

#ifndef VELOCITYESTIMATOR_CUH_
#define VELOCITYESTIMATOR_CUH_

#include "gpuBF.cuh"

namespace gpuBF {

template <typename T_in, typename T_out>
class VelocityEstimator: public DataProcessor<T_out> {
private:

	// CUDA objects
	cudaTextureObject_t tex;
	cudaTextureDesc texDesc;
	cudaResourceDesc resDesc;
	
	// Input
	T_in *d_dat;
	gpuArrayDesc datDesc; // Description of data
	int ensSize, maxlag;
	float scaleFactor;
	int halfks_z, halfks_x;
	bool useChanData;
	
	// Need an intermediate buffer to store numerator and denominator
	// of running sums to compute velocity estimates
	int *d_num, *d_den;
	gpuArrayDesc sumDesc; // Description of sums

	// Functions
	void initValuesToZero();
	void initializeArrays();

public:
	VelocityEstimator();
	virtual ~VelocityEstimator();
	void initialize(DataProcessor<T_in> *F,
	                int ensembleLength,
	                float velocityScaleFactor,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	void initialize(T_in *d_focused,
					gpuArrayDesc desc,
					int ensembleLength,
					float velocityScaleFactor,
					int deviceID,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	void reset();

	void setInputDevPtr(T_in *d_AnalyticData);
	void setMaximumLag(int maximumLag);
	void setHalfKernel(int halfKernel_z, int halfKernel_x);
	void setScaleFactor(float velocityScaleFactor);
	void estimateVelocity_Kasai();
	void estimateVelocity_chanKasai();

};

template <typename T_in>
__global__ void computeNumAndDen_Kasai_kernel(T_in *dat,
                                              int datSize_w,
                                              int ensSize,
                                              int *num,
                                              int *den,
                                              int4 sumSize);

template <typename T_in>
__global__ void computeNumAndDen_chanKasai_kernel(T_in *dat,
                                                  int datSize_w,
                                                  int ensSize,
                                                  int maxlag,
                                                  int *num,
                                                  int *den,
                                                  int4 sumSize);

template <typename T_out>
__global__ void estimateVelocity_Kasai_kernel(int *num,
                                              int *den,
                                              int4 sumSize,
                                              int halfks_z,
                                              int halfks_x,
                                              float scaleFactor,
                                              T_out *img,
                                              int imgSize_w);

template <typename T_out>
__global__ void estimateVelocity_chanKasai_kernel(int *num,
                                                  int *den,
                                                  int4 sumSize,
                                                  int halfks_z,
                                                  int halfks_x,
                                                  float scaleFactor,
                                                  T_out *img,
                                                  int imgSize_w);
}

#endif /* VELOCITYESTIMATOR_CUH_ */
