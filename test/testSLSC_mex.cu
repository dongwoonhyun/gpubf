#include "gpuBF.cuh"
#include "SLSC.cuh"
#include <mex.h>

using gpuBF::gpuArrayDesc;
using gpuBF::makePitchedArray;

template <typename T_out>
__global__ void convertType2ToFloat2(T_out *tmp, int4 tmpSize, float *dat, int datSize_w) {
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;

	if (row < tmpSize.x && chan < tmpSize.y && col < tmpSize.z) {
		int iidx = row + tmpSize.w*(chan + tmpSize.y*col);
		int oidx = row + datSize_w*(chan + tmpSize.y*col);
		dat[oidx] = loadData(tmp, iidx);
	}
}
template <typename T_out>
void copyResultsToHost(T_out *d_tmp, gpuArrayDesc tmpDesc, float *h_dat) {
	gpuArrayDesc datDesc = tmpDesc;
	datDesc.typeSize = sizeof(float2);
	float *d_dat;
	makePitchedArray((void **)&d_dat, &datDesc);
	dim3 B(256, 1, 1);
	dim3 G((datDesc.size.x-1)/B.x+1, (datDesc.size.y-1)/B.y+1, (datDesc.size.z-1)/B.z+1);
	convertType2ToFloat2<<<G,B>>>(d_tmp, tmpDesc.size, d_dat, datDesc.size.w);
	CCE(cudaMemcpy2D(h_dat,
	                 datDesc.typeSize*datDesc.size.x,
	                 d_dat,
	                 datDesc.pitchInBytes,
	                 datDesc.typeSize*datDesc.size.x,
	                 datDesc.size.y*datDesc.size.z,
	                 DtoH));
	CCE(cudaFree(d_dat));
}

template <typename T_in, typename T_out>
void executeTest(mxArray *plhs[], int runID, int nrhs, const mxArray *prhs[]) {

	int devID = 0;
	CCE(cudaSetDevice(devID));
	// CCE(cudaDeviceSetCacheConfig(cudaFuncCachePreferL1));

	int gpuverb = (int)mxGetScalar(prhs[3]);
	// Parse data dimensions
	const mwSize *dims = mxGetDimensions(prhs[0]);
	int dataSize = mxGetNumberOfElements(prhs[0]);
	if (gpuverb > 0)
		printf("%d\t%dx%dx%d\n",dataSize, dims[0], dims[1], dims[2]);

	// Interleave real and complex data into a host array
	float *h_I = (float *)mxGetData(prhs[0]);
	float *h_Q = (float *)mxGetImagData(prhs[0]);

	// Copy the data onto a host array
	T_in *h_foc = (T_in *)mxMalloc(sizeof(T_in)*dataSize);
	for (int i = 0; i < dataSize; i++) {
		h_foc[i].x = h_I[i];
		h_foc[i].y = h_Q[i];
	}
	// Create a GPU array description for the data
	gpuArrayDesc desc;
	desc.size.x = dims[0];
	desc.size.y = dims[1];
	desc.size.z = dataSize/dims[0]/dims[1];
	desc.typeSize = sizeof(T_in);

	// Allocate a GPU array and copy data to it
	T_in *d_foc;
	makePitchedArray((void **)&d_foc, &desc);
	if (gpuverb > 1) gpuBF::printDesc(devID, desc, "Input");
	CCE(cudaMemcpy2D(d_foc,
	    			 desc.pitchInBytes,
	    			 h_foc,
	    			 desc.typeSize*desc.size.x,
	    			 desc.typeSize*desc.size.x,
	    			 desc.size.y*desc.size.z,
	    			 HtoD));

	// Initialize SLSC object
	gpuBF::SLSC<T_in,T_out> C;
	C.initialize(d_foc, desc, devID, 0, gpuverb);
	C.setMaximumLag((int)mxGetScalar(prhs[1]));
	C.setCCMode((int)mxGetScalar(prhs[2]));

	// Compute SLSC
	mexEvalString("tic;");
	C.makeSLSC();
	mexEvalString("tGPU(i) = toc;");
	CCE(cudaDeviceSynchronize());

	// Copy back data
	gpuArrayDesc imgDesc = C.getOutputDesc();
	size_t imgsz[3] = {imgDesc.size.x, imgDesc.size.y, imgDesc.size.z};
	plhs[runID] = mxCreateNumericArray(3, imgsz, mxSINGLE_CLASS, mxREAL);
	CCE(cudaMemcpy2D((float *)mxGetData(plhs[runID]),
	    			 imgDesc.typeSize*imgsz[0],
	    			 C.getOutputDevPtr(),
	    			 imgDesc.pitchInBytes,
	    			 imgDesc.typeSize*imgsz[0],
	    			 imgsz[1]*imgsz[2],
	    			 DtoH));

	mxFree(h_foc);
	C.reset();

	CCE(cudaSetDevice(devID));
	CCE(cudaDeviceReset());
}


// Gateway function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	if (nrhs != 4)
		mexErrMsgTxt("Inputs must be: data, maxlag, ccmode, gpuverb.\n");
	if (mxGetClassID(prhs[0]) != mxSINGLE_CLASS || !mxIsComplex(prhs[0]))
		mexErrMsgTxt("Make sure the input data is of type 'single complex'.\n");

	mexEvalString("i = 1;"); executeTest<float2,float>(plhs, 0, nrhs, prhs);
	mexEvalString("i = 2;"); executeTest<short2,float>(plhs, 1, nrhs, prhs);
	mexEvalString("i = 3;"); executeTest<int2,  float>(plhs, 2, nrhs, prhs);
}
