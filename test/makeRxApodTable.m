%% MATLAB focusing kernel
function apoRx = makeRxApodTable(elemPos, pixPos, fnum, apers_0)

if nargin == 4 && length(unique(apers_0)) == 1
	elemPos = elemPos( int32(1:128) + apers_0(1), :);
end

nelems = size(elemPos,1);
nrows = size(pixPos,1);
ncols = size(pixPos,2);
apoRx = zeros(nrows,nelems,ncols,'single');

zpos = pixPos(:,1,3);
xpos = pixPos(1,:,1);

for E = 1:nelems
	epos = elemPos(E,:);
	idx = abs( bsxfun(@rdivide, zpos, (epos(1)-xpos)) ) > fnum;
	% Set apodization of valid indices to 1, invalid indices to 1e-4.
	% This avoids having a NaN issue later on with SLSC.
	apo = zeros(nrows, ncols, 'single');
	apo(idx) = 1;
	apo(~idx) = 1e-4;
	apoRx(:,E,:) = apo;
end
