#include "gpuBF.cuh"
#include "Focus.cuh"
#include <mex.h>

using gpuBF::gpuArrayDesc;
using gpuBF::saveData2;
using gpuBF::loadData2;
using gpuBF::saveData;
using gpuBF::loadData;

template <typename T_out>
__global__ void convertType2ToFloat2(T_out *tmp, int4 tmpSize, float2 *foc, int focSize_w) {
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;

	if (row < tmpSize.x && chan < tmpSize.y && col < tmpSize.z) {
		int iidx = row + tmpSize.w*(chan + tmpSize.y*col);
		int oidx = row + focSize_w*(chan + tmpSize.y*col);
		saveData2(foc, oidx, loadData2(tmp, iidx));
	}
}
template <typename T_out>
void copyResultsToHost(T_out *d_tmp, gpuArrayDesc tmpDesc, float2 *h_foc) {
	gpuBF::gpuArrayDesc focDesc = tmpDesc;
	focDesc.typeSize = sizeof(float2);
	float2 *d_foc;
	gpuBF::makePitchedArray((void **)&d_foc, &focDesc);
	dim3 B(256, 1, 1);
	dim3 G((focDesc.size.x-1)/B.x+1, (focDesc.size.y-1)/B.y+1, (focDesc.size.z-1)/B.z+1);
	convertType2ToFloat2<<<G,B>>>(d_tmp, tmpDesc.size, d_foc, focDesc.size.w);
	CCE(cudaMemcpy2D(h_foc,
	                 focDesc.typeSize*focDesc.size.x,
	                 d_foc,
	                 focDesc.pitchInBytes,
	                 focDesc.typeSize*focDesc.size.x,
	                 focDesc.size.y*focDesc.size.z,
	                 DtoH));
	CCE(cudaFree(d_foc));
}
// template <>
// void copyResultsToHost(float2 *d_foc, gpuArrayDesc focDesc, float2 *h_foc) {
// 	CCE(cudaMemcpy2D(h_foc,
// 	                 focDesc.typeSize*focDesc.size.x,
// 	                 d_foc,
// 	                 focDesc.pitchInBytes,
// 	                 focDesc.typeSize*focDesc.size.x,
// 	                 focDesc.size.y*focDesc.size.z,
// 	                 DtoH));	
// }

template <typename T_in, typename T_out>
void testFocusDataTypes(float *h_I, float *h_Q, int dataSize,
                        int nrows, int nchans, int ncols, int nxmits,
                        float *h_delRx, float *h_apodRx,
                        float *h_delTx, float *h_apodTx,
                        float spcy, float wlps, bool isBaseband,
                        const mwSize *dims, float *h_outI, float *h_outQ) {

	// Interleave real and complex data into a host array
	T_in *h_raw = (T_in *)mxMalloc(sizeof(T_in)*dataSize);
	for (int i = 0; i < dataSize; i++) {
		h_raw[i].x = h_I[i];
		h_raw[i].y = h_Q[i];
	}

	// Create a GPU array description for the data
	gpuBF::gpuArrayDesc rawDesc;
	rawDesc.size.x = dims[0];
	rawDesc.size.y = dims[1];
	rawDesc.size.z = dataSize/dims[0]/dims[1];
	rawDesc.typeSize = sizeof(T_in);
	if ( (h_delTx != NULL || h_apodTx != NULL) && rawDesc.size.z != nxmits)
		mexErrMsgTxt("The data and tables have different nxmits.\n");

	// Allocate a GPU array and copy data to it
	T_in *d_raw;
	gpuBF::makePitchedArray((void **)&d_raw, &rawDesc);
	CCE(cudaMemcpy2D(d_raw,
	    			 rawDesc.pitchInBytes,
	    			 h_raw,
	    			 rawDesc.typeSize*rawDesc.size.x,
	    			 rawDesc.typeSize*rawDesc.size.x,
	    			 rawDesc.size.y*rawDesc.size.z,
	    			 HtoD));

	// Initialize Focus object
	gpuBF::Focus<T_in,T_out> F;
	F.initialize(d_raw, rawDesc, 0, 0, 0);

	// Load Tx and Rx delay and apodization tables
	F.loadRxTables(h_delRx, h_apodRx, nrows, nchans, ncols);
	F.loadTxTables(h_delTx, h_apodTx, nrows, nxmits, ncols);

	// Set focusing parameters
	F.setSamplesPerCycle(spcy);
	F.setOutputWlps(wlps);

	mexEvalString("tic;");
	// Focus data
	if (isBaseband) { F.focusBasebandData();  }
	else 			{ F.focusModulatedData(); }
	mexEvalString("tGPU(i) = toc;");
	CCE(cudaDeviceSynchronize());

	// Read back the output
	gpuBF::gpuArrayDesc focDesc = F.getOutputDesc();
	float2 *h_foc = (float2 *)mxMalloc(sizeof(float2)*nrows*nchans*ncols);
	copyResultsToHost(F.getOutputDevPtr(), focDesc, h_foc);

	// Compute error
	for (int k = 0; k < ncols; k++) {
		for (int j = 0; j < nchans; j++) {
			for (int i = 0; i < nrows; i++) {
				int idx = i + nrows*(j + nchans*k);
				h_outI[idx] = h_foc[idx].x;
				h_outQ[idx] = h_foc[idx].y;
			}
		}
	}

	CCE(cudaSetDevice(0));
	mxFree(h_foc);
	mxFree(h_raw);
	CCE(cudaFree(d_raw));
	F.reset();
	CCE(cudaDeviceReset());
}


// Gateway function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	int devID = 0;
	CCE(cudaSetDevice(devID));

	if (nrhs != 8)
		mexErrMsgTxt("Inputs must be: data, delRx, apodRx, delTx, apodTx, inSpcy, outWlps, and isBaseband.\n");
	if (mxGetClassID(prhs[0]) != mxSINGLE_CLASS || !mxIsComplex(prhs[0]))
		mexErrMsgTxt("Make sure the input data is of type 'single complex'.\n");

	// Parse data dimensions
	const mwSize *dims = mxGetDimensions(prhs[0]);
	int dataSize = mxGetNumberOfElements(prhs[0]);

	// Parse delay and apodization tables
	float *h_delRx, *h_apodRx, *h_delTx, *h_apodTx;
	h_delRx  = mxIsEmpty(prhs[1]) ? NULL : (float *)mxGetData(prhs[1]);
	h_apodRx = mxIsEmpty(prhs[2]) ? NULL : (float *)mxGetData(prhs[2]);
	h_delTx  = mxIsEmpty(prhs[3]) ? NULL : (float *)mxGetData(prhs[3]);
	h_apodTx = mxIsEmpty(prhs[4]) ? NULL : (float *)mxGetData(prhs[4]);
	if (h_delRx == NULL) mexErrMsgTxt("The receive delay profile is required.\n");

	// Get data dimensions
	int nrows, ncols, nchans, nxmits;
	const mwSize *d = mxGetDimensions(prhs[1]);
	nrows = d[0]; nchans = d[1]; ncols = d[2];
	if (h_delTx != NULL || h_apodTx != NULL) {
		d = mxGetDimensions(prhs[3]);
		nxmits = d[1];
	}

	float *h_I = (float *)mxGetData(prhs[0]);
	float *h_Q = (float *)mxGetImagData(prhs[0]);
	float spcy = (float)mxGetScalar(prhs[5]);
	float wlps = (float)mxGetScalar(prhs[6]);
	bool isBaseband = (bool)mxGetScalar(prhs[7]);

	// The following type combinations will be tested:
	// 1. float2=>float2
	// 2. short2=>float2
	// 3. short2=>half2
	size_t focsz[3] = {nrows, nchans, ncols};
	float *h_outI, *h_outQ;

	// 1. float2=>float2
	plhs[0] = mxCreateNumericArray(3, focsz, mxSINGLE_CLASS, mxCOMPLEX);
	h_outI = (float *)mxGetData(plhs[0]);
	h_outQ = (float *)mxGetImagData(plhs[0]);

	mexEvalString("tGPU = zeros(4,1);");
	mexEvalString("i = 1;");
	testFocusDataTypes<float2,float2>(h_I, h_Q, dataSize, nrows, nchans, ncols, nxmits,
	                                  h_delRx, h_apodRx, h_delTx, h_apodTx, spcy, wlps,
	                                  isBaseband, dims, h_outI, h_outQ);

	// 2. short2=>float2
	mexEvalString("i = 2;");
	plhs[1] = mxCreateNumericArray(3, focsz, mxSINGLE_CLASS, mxCOMPLEX);
	h_outI = (float *)mxGetData(plhs[1]);
	h_outQ = (float *)mxGetImagData(plhs[1]);
	testFocusDataTypes<short2,float2>(h_I, h_Q, dataSize, nrows, nchans, ncols, nxmits,
	                                  h_delRx, h_apodRx, h_delTx, h_apodTx, spcy, wlps,
	                                  isBaseband, dims, h_outI, h_outQ);

	// 3. short2=>half2
	mexEvalString("i = 3;");
	plhs[2] = mxCreateNumericArray(3, focsz, mxSINGLE_CLASS, mxCOMPLEX);
	h_outI = (float *)mxGetData(plhs[2]);
	h_outQ = (float *)mxGetImagData(plhs[2]);
	testFocusDataTypes<short2,half2>(h_I, h_Q, dataSize, nrows, nchans, ncols, nxmits,
	                                 h_delRx, h_apodRx, h_delTx, h_apodTx, spcy, wlps,
	                                 isBaseband, dims, h_outI, h_outQ);

	// 4. short2=>half2
	mexEvalString("i = 4;");
	plhs[3] = mxCreateNumericArray(3, focsz, mxSINGLE_CLASS, mxCOMPLEX);
	h_outI = (float *)mxGetData(plhs[3]);
	h_outQ = (float *)mxGetImagData(plhs[3]);
	testFocusDataTypes<short2,short2>(h_I, h_Q, dataSize, nrows, nchans, ncols, nxmits,
	                                 h_delRx, h_apodRx, h_delTx, h_apodTx, spcy, wlps,
	                                 isBaseband, dims, h_outI, h_outQ);

}