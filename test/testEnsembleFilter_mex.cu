#include "gpuBF.cuh"
#include "EnsembleFilter.cuh"
#include <mex.h>

using gpuBF::FocType;
using gpuBF::FocType2;

// Gateway function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	int devID = 0;
	CCE(cudaSetDevice(devID));
	// CCE(cudaDeviceSetCacheConfig(cudaFuncCachePreferL1));

	if (nrhs != 3)
		mexErrMsgTxt("Inputs must be: data, filtcoefs, gpuverb.\n");
	if (mxGetClassID(prhs[0]) != mxSINGLE_CLASS || !mxIsComplex(prhs[0]))
		mexErrMsgTxt("Make sure the input data is of type 'single complex'.\n");
	if (mxGetClassID(prhs[1]) != mxSINGLE_CLASS || mxIsComplex(prhs[1]))
		mexErrMsgTxt("Make sure the filter coefficients is of type 'single real'.\n");
	if (mxGetNumberOfDimensions(prhs[0]) < 4)
		mexErrMsgTxt("Input must be 4-D: samps x chans x beams x tracks.\n");

	int gpuverb = (int)mxGetScalar(prhs[2]);
	// Parse data dimensions
	const mwSize *dims = mxGetDimensions(prhs[0]);
	int dataSize = mxGetNumberOfElements(prhs[0]);
	if (gpuverb > 0)
		printf("%d\t%dx%dx%dx%d\n",dataSize, dims[0], dims[1], dims[2], dims[3]);

	// Interleave real and complex data into a host array
	FocType2 *h_foc = (FocType2 *)mxMalloc(sizeof(FocType2)*dataSize);
	float *h_I = (float *)mxGetData(prhs[0]);
	float *h_Q = (float *)mxGetImagData(prhs[0]);
	for (int i = 0; i < dataSize; i++) {
		h_foc[i].x = (FocType)h_I[i];
		h_foc[i].y = (FocType)h_Q[i];
	}
	float *h_filt = (float *)mxGetData(prhs[1]);

	// Create a GPU array description for the data
	gpuBF::gpuArrayDesc desc;
	desc.size.x = dims[0];
	desc.size.y = dims[1];
	desc.size.z = dataSize/dims[0]/dims[1];
	desc.typeSize = sizeof(float2);

	// Allocate a GPU array and copy data to it
	FocType2 *d_foc;
	gpuBF::makePitchedArray((void **)&d_foc, &desc);
	if (gpuverb > 1) gpuBF::printDesc(devID, desc, "Input");
	CCE(cudaMemcpy2D(d_foc,
	    			 desc.pitchInBytes,
	    			 h_foc,
	    			 desc.typeSize*desc.size.x,
	    			 desc.typeSize*desc.size.x,
	    			 desc.size.y*desc.size.z,
	    			 HtoD));
	mexEvalString("tic");
	
	// Change desc so that it only refers to one track rather than the full ensemble
	desc.size.z = dims[2];

	// Initialize EnsembleFilter object
	gpuBF::EnsembleFilter EF;
	EF.initialize(d_foc, desc, dims[3], h_filt, devID, 0, gpuverb);

	// Apply high-pass filter across ensemble
	EF.applyEnsembleFilter();

	CCE(cudaDeviceSynchronize());
	mexEvalString("tGPU = toc;");
	// Copy back data
	desc.size.z = dataSize/dims[0]/dims[1];
	CCE(cudaMemcpy2D(h_foc,
	    			 desc.typeSize*desc.size.x,
	    			 d_foc,
	    			 desc.pitchInBytes,
	    			 desc.typeSize*desc.size.x,
	    			 desc.size.y*desc.size.z,
	    			 DtoH));

	// Un-interleave data
	plhs[0] = mxCreateNumericArray(mxGetNumberOfDimensions(prhs[0]),
	                               dims,
	                               mxSINGLE_CLASS,
	                               mxCOMPLEX);
	h_I = (float *)mxGetData(plhs[0]);
	h_Q = (float *)mxGetImagData(plhs[0]);
	for (int i = 0; i < dataSize; i++) {
		h_I[i] = h_foc[i].x;
		h_Q[i] = h_foc[i].y;
	}
	mxFree(h_foc);
	EF.reset();


	CCE(cudaSetDevice(devID));
	CCE(cudaDeviceReset());

}
