%% MATLAB function to compute CCs
function ccMAT = testSLSC_mat(data, maxlag, ccmode)
[nrows, nchans, ncols] = size(data);
if ccmode == 0 % ensemble version
	numer = zeros(nrows, ncols);
	deno1 = zeros(nrows, ncols);
	deno2 = zeros(nrows, ncols);
	for m = 1:maxlag
		for i = 1:nchans-m
			a = squeeze(data(:,i,:));
			b = squeeze(data(:,i+m,:));
			numer = numer + real(a).*real(b) + imag(a).*imag(b);
			deno1 = deno1 + real(a).*real(a) + imag(a).*imag(a);
			deno2 = deno2 + real(b).*real(b) + imag(b).*imag(b);
		end
	end
	ccMAT = single( numer ./ sqrt(deno1) ./ sqrt(deno2) );
else % averaged version
	ccsum = zeros(nrows, ncols);
	numcc = 0;
	for m = 1:maxlag
		for i = 1:nchans-m
			numcc = numcc + 1;
			a = squeeze(data(:,i,:));
			b = squeeze(data(:,i+m,:));
			numer = real(a).*real(b) + imag(a).*imag(b);
			deno1 = real(a).*real(a) + imag(a).*imag(a);
			deno2 = real(b).*real(b) + imag(b).*imag(b);
			ccsum = ccsum + numer ./ sqrt(deno1) ./ sqrt(deno2);
		end
	end
	ccMAT = single( ccsum / numcc );
end
end
