%% MATLAB focusing kernel
function [delRx, delTx] = makeDelayTables(elemPos, pixPos, ...
	beamOriTx, beamDirTx, spwlTx, time0wl, apers_0)

spcyTx = spwlTx/2;

if nargin == 7 && length(unique(apers_0)) == 1
	elemPos = elemPos( int32(1:128) + apers_0(1), :);
end

nelems = size(elemPos,1);
nrows = size(pixPos,1);
ncols = size(pixPos,2);

% First, compute distance to pixel positions for each channel
delRx = zeros(nrows, nelems, ncols, 'single');
for C = 1:ncols
	tmp = bsxfun(@minus, pixPos(:,C,:), permute(elemPos, [3 1 2]));
	delRx(:,:,C) = sqrt(sum(tmp.^2,3));
end

% Then compute the delays for the Tx component
if ~isempty(beamOriTx) && ~isempty(beamDirTx) % Focused transmit
	disp('Focused transmit')
	% Assume we are using a 1:1 Tx:Rx pattern with no compounding
	% (i.e., nxmits == ncols)
	if ncols ~= size(beamOriTx,1) || ncols ~= size(beamDirTx,1)
		error(['Only 1:1 Tx:Rx is supported (so far) when Tx ' ...
			'beam origin and direction are both provided.'])
	end
	% Get relative position from the beam origin to the pixel position
	tmp = bsxfun(@minus, pixPos, permute(beamOriTx, [3 1 2]));
	% Compute the Euclidean distance (units are in wavelengths)
	dwl = sqrt(sum(tmp.^2,3));
	% Add a transmit delay corresponding to time of flight to focal point
	delRx = bsxfun(@plus, delRx, permute(dwl,[1 3 2]));
	% Also add the time zero in wavelengths
	delRx = bsxfun(@plus, delRx, permute(time0wl,[2 3 1]));
	delTx = [];
	
elseif ~isempty(beamDirTx) % Plane wave transmit
	% Initialize the outputs
	nxmits = size(beamDirTx,1);
	delTx = zeros(nrows, nxmits, ncols, 'single');
	% Compute the transmit delays
	for X = 1:nxmits
		theta = beamDirTx(X,1);
		for C = 1:ncols
			xpos = pixPos(:,C,1);
			zpos = pixPos(:,C,3);
			delTx(:,X,C) = time0wl(X) + ...
				zpos*cos(theta) + xpos*sin(theta);
% 			delTx(:,X,C) = time0wl(X) + ...
% 				pixPos(:,C,3) / cos(beamDirTx(X,1)) + ...
% 				pixPos(:,C,1) * sin(beamDirTx(X,1));
		end
	end
	
elseif ~isempty(beamOriTx) % Diverging wave transmit
	% Initialize the outputs
	nxmits = size(beamOriTx,1);
	delTx = zeros(nrows, nxmits, ncols, 'single');
	% Compute the transmit delays
	for X = 1:nxmits
		for C = 1:ncols
			xpos = pixPos(:,C,1);
			ypos = pixPos(:,C,2);
			zpos = pixPos(:,C,3);
			delTx(:,X,C) = time0wl(X) + sqrt( ...
				(beamOriTx(X,1)-xpos).^2 + ...
				(beamOriTx(X,2)-ypos).^2 + ...
				(beamOriTx(X,3)-zpos).^2);
		end
	end
else
	error('Specify the Tx beam origin and/or direction.')
end
% Convert from wavelengths to samples @ Tx sampling freq
delRx = delRx * spcyTx;
delTx = delTx * spcyTx;

end





% 
% 
% 
% 
% % Get data size info
% nchans = size(elemPos,1);
% nxmits = max(size(beamOriTx,1), size(beamDirTx,1));
% elemPos = permute(elemPos, [3 1 2]);
% if isempty(spwlRx)
% 	spwlRx = spwlTx;
% end
% 
% if isempty(beamOriRx) && isempty(beamDirRx) % 1-to-1 reconstruction
% 	% Initialize the outputs
% 	ncols = nxmits;
% 	delRxMAT = zeros(nrows, nchans, ncols, 'single');
% 	delTxMAT = [];
% 	% Compute the pixel positions
% 	pixPos = computePixelPositions(beamOriTx, beamDirTx, nrows, spwlRx);
% 	% Compute distance to pixel positions for each channel
% 	for C = 1:ncols
% 		tmp = bsxfun(@minus, pixPos(:,C,:), elemPos);
% 		delRxMAT(:,:,C) = sqrt(sum(tmp.^2,3));
% 	end
% % 	% Add a transmit delay corresponding to time of flight to focal point
% % 	delRxMAT = bsxfun(@plus, delRxMAT, (1:nrows)'/spwlRx);
% % 	% Add time zero in wavelengths
% % 	delRxMAT = bsxfun(@plus, delRxMAT, permute(time0wl,[2 3 1]));
% 	
% elseif isempty(beamOriTx) % Plane wave reconstruction
% 	% Initialize the outputs
% 	ncols = size(beamDirRx,1);
% 	delRxMAT = zeros(nrows, nchans, ncols, 'single');
% 	delTxMAT = zeros(nrows, nxmits, ncols, 'single');
% 	% Compute the pixel positions
% 	pixPos = computePixelPositions(beamOriRx, beamDirRx, nrows, spwlRx);
% 	% Compute the distance to pixel positions for each channel
% 	for C = 1:ncols
% 		tmp = bsxfun(@minus, pixPos(:,C,:), elemPos);
% 		delRxMAT(:,:,C) = sqrt(sum(tmp.^2,3));
% 	end
% 	% Compute the transmit delays
% 	for X = 1:nxmits
% 		for C = 1:ncols
% 			delTxMAT(:,X,C) = time0wl(X) + ...
% 				pixPos(:,C,3) / cos(beamDirTx(X,1)) + ...
% 				pixPos(:,C,1) * sin(beamDirTx(X,1));
% 		end
% 	end
% 	
% elseif isempty(beamDirTx) % Diverging wave reconstruction
% 	% Initialize the outputs
% 	ncols = size(beamOriRx,1);
% 	delRxMAT = zeros(nrows, nchans, ncols, 'single');
% 	delTxMAT = zeros(nrows, nxmits, ncols, 'single');
% 	% Compute the pixel positions
% 	pixPos = computePixelPositions(beamOriRx, beamDirRx, nrows, spwlRx);
% 	% Compute distance to pixel positions for each channel
% 	for C = 1:ncols
% 		tmp = bsxfun(@minus, pixPos(:,C,:), elemPos);
% 		delRxMAT(:,:,C) = sqrt(sum(tmp.^2,3));
% 	end
% 	% Compute the transmit delays
% 	for X = 1:nxmits
% 		for C = 1:ncols
% 			tmp = bsxfun(@minus, pixPos(:,C,:), permute(beamOriRx(X,:),[3 1 2]));
% 			delTxMAT(:,X,C) = sqrt(sum(tmp.^2,3)) + time0wl(X);
% 		end
% 	end
% end
% 
% % Convert from wavelengths to samples @ Tx sampling freq
% delRxMAT = delRxMAT * spwlTx;
% delTxMAT = delTxMAT * spwlTx;
% delRxMAT = elemPos;%pixPos;
% end

