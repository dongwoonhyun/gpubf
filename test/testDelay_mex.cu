#include "gpuBF.cuh"
#include "Delay.cuh"
#include <mex.h>

// Gateway function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	int devID = 0;
	CCE(cudaSetDevice(devID));

	if (nrhs != 10)
		mexErrMsgTxt("Must have 10 inputs.\n");
	for (int i = 0; i < nrhs; i++) {
		if (mxGetClassID(prhs[i]) != mxSINGLE_CLASS)
			mexErrMsgTxt(sprintf("Make sure input %d is of class single.\\n",i));
	}

	// Load inputs
	float *elemPos = (float *)mxGetData(prhs[0]);
	float *oriAcq  = (float *)mxGetData(prhs[1]);
	float *dirAcq  = (float *)mxGetData(prhs[2]);
	float spwlAcq  = (float)mxGetScalar(prhs[3]);
	float *time0wl = (float *)mxGetData(prhs[4]);
	float *oriRcn  = (float *)mxGetData(prhs[5]);
	float *dirRcn  = (float *)mxGetData(prhs[6]);
	float spwlRcn  = (float)mxGetScalar(prhs[7]);
	int nrows      = ( int )mxGetScalar(prhs[8]);
	float fNumTx   = (float)mxGetScalar(prhs[9]);
	float fNumRx   = (float)mxGetScalar(prhs[10]);
	int gpuverb    = ( int )mxGetScalar(prhs[11]);

	// Parse data dimensions
	int nchans = mxGetM(prhs[0]);
	int nxmits = max(mxGetM(prhs[1]), mxGetM(prhs[2]));
	int ncols  = mxGetM(prhs[5]);
	gpuBF::gpuArrayDesc RxDesc, TxDesc;
	RxDesc.typeSize = sizeof(float);
	TxDesc.typeSize = sizeof(float);
	RxDesc.size = int4(nrows, nchans, ncols, 0);
	TxDesc.size = int4(nrows, nxmits, ncols, 0);

	// If receive beam data is not provided, assume 1 to 1
	if (mxIsEmpty(prhs[5]) || mxIsEmpty(prhs[6])) {
		TxDesc.size = int4(0, 0, 0, 0);
		oriRcn = NULL;
		dirRcn = NULL;
	}
	else {
		// If only beam directions are provided
		if (mxIsEmpty(prhs[1]) && !mxIsEmpty(prhs[2]))
			oriAcq = NULL;
		else if (!mxIsEmpty(prhs[1]) && mxIsEmpty(prhs[2]))
			dirAcq = NULL;
		else
			mexErrMsgTxt(sprintf("%s%s\\n",
			    "For synthetic aperture reconstruction, must provide ",
			    "either Tx beam origin or direction.");
	}

	// Initialize Delay object on the GPU
	gpuBF::Delay D;
	D.initialize(RxDesc, TxDesc, devID, 0, gpuverb);

	// Set acquisition and reconstruction parameters
	D.setAcqParams(oriAcq, dirAcq, spwlAcq, time0wl, elemPos);
	D.setRcnParams(oriRcn, dirRcn, spwlRcn);

	// Also set desired Tx or Rx F/#
	D.setApodParams(fNumTx, fNumRx);

	// Compute delay tables
	D.computeDelayTables();
	D.ComputeApodTables();

	// Copy back data
	gpuBF::gpuArrayDesc focDesc = F.getOutputDesc();
	size_t focsz[3] = {2*focDesc.size.x, focDesc.size.y, focDesc.size.z};
	plhs[0] = mxCreateNumericArray(3, focsz, mxSINGLE_CLASS, mxCOMPLEX);
	CCE(cudaMemcpy2D((float2 *)mxGetData(plhs[0]),
	    			 sizeof(float)*focsz[0],
	    			 F.getOutputDevPtr(),
	    			 focDesc.pitchInBytes,
	    			 sizeof(float)*focsz[0],
	    			 focsz[1]*focsz[2],
	    			 DtoH));

	mxFree(h_raw);
	F.reset();

	CCE(cudaSetDevice(devID));
	CCE(cudaDeviceReset());

}
