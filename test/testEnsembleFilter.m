function result = testEnsembleFilter()

%% Testing parameters
verb = 0; gpuverb = 0;
% Set thresholds for errors
msqthresh = 1e-8; % Error threshold for msq error
maxthresh = 1e-4;   % Error threshold for any one value
ntracks = 16;
nruns = 5;
result = true; % Initial value

%% Load data set
data = [];
for i = 1:ntracks
	dataFile = ['/data/dhyun/simulation/lesions_vf10-5/data/focdata/cysts-12dB_' ...
		num2str(i) '/cysts-12dB_' num2str(i) '.mat'];
	data = cat(4, data, loadData(dataFile));
end
% data = data(1:64,1:64,1:64,:);

%% Run tests
fprintf('\n  Run  ||  Result  |  Msq. Err  |  Max. Err  |  CPU Time  |  GPU Time  ');
fprintf('\n-------||----------|------------|------------|------------|------------');
fprintf('\n');
for i = 1:nruns
	fprintf('   %2d  || ', i);
	res = runTest(data, verb, gpuverb);
	result = result && res;
end

end

%% Function to load data
function data = loadData(dataFile)
if exist(dataFile,'file')
	disp(['Loading ' dataFile '...'])
	load(dataFile, 'focsig');
	data = permute(focsig, [1 3 2]);
	data = squeeze(sum(reshape(data,size(data,1),2,[],size(data,3)),2));
else
	nrows = 256;
	nchans = 128;
	ncols = 64;
	data = 256*randn(nrows, nchans, ncols, 'single') + ...
		1i*256*randn(nrows, nchans, ncols, 'single');
end

% Normalize data
data = data / max(abs(data(:)));
end

%% Run the test for a given set of tables
function [result, msqerr, maxerr] = ...
	runTest(data, verb, gpuverb)
msqthresh = evalin('caller','msqthresh'); % Error threshold for msq error
maxthresh = evalin('caller','maxthresh'); % Error threshold for any one value
result = true;

% Generate random filter coefficients
filtcoefs = rand(size(data,4),'single');

% MATLAB version
tic
ccMAT = testEnsembleFilter_mat(data, filtcoefs);
tMAT = toc;

% GPU version
% tic
ccGPU = testEnsembleFilter_mex(data, filtcoefs, gpuverb);
% tGPU = toc; % obtain tGPU from within the mex function

% Measure error
msqerr = mean(abs(ccMAT(:)-ccGPU(:)).^2);
maxerr = max( abs(ccMAT(:)-ccGPU(:)));
if msqerr < msqthresh && maxerr < maxthresh
	fprintf('Success! ');
else
	fprintf('Failure! ');
	result = false;
end
fprintf('|  %8.2E  ', msqerr)
fprintf('|  %8.2E  ', maxerr)
fprintf('|  %8f  |  %8f  ', tMAT, tGPU)

if verb > 0
	if msqerr > msqthresh
		disp('Mean square error is too large.')
	end
	if maxerr > maxthresh
		disp('Maximum error is too large.')
	end
end

fprintf('\n');
end

