%% MATLAB focusing kernel
function [delRxMAT, delTxMAT, pixPos] = testDelay_mat(elemPos, ...
	beamOriTx, beamDirTx, spwlTx, time0wl, ...
	beamOriRx, beamDirRx, spwlRx, nrows)

% Get data size info
nchans = size(elemPos,1);
nxmits = max(size(beamOriTx,1), size(beamDirTx,1));
elemPos = permute(elemPos, [3 1 2]);
if isempty(spwlRx)
	spwlRx = spwlTx;
end

if isempty(beamOriRx) && isempty(beamDirRx) % 1-to-1 reconstruction
	% Initialize the outputs
	ncols = nxmits;
	delRxMAT = zeros(nrows, nchans, ncols, 'single');
	delTxMAT = [];
	% Compute the pixel positions
	pixPos = computePixelPositions(beamOriTx, beamDirTx, nrows, spwlRx);
	% Compute distance to pixel positions for each channel
	for C = 1:ncols
		tmp = bsxfun(@minus, pixPos(:,C,:), elemPos);
		delRxMAT(:,:,C) = sqrt(sum(tmp.^2,3));
	end
% 	% Add a transmit delay corresponding to time of flight to focal point
% 	delRxMAT = bsxfun(@plus, delRxMAT, (1:nrows)'/spwlRx);
% 	% Add time zero in wavelengths
% 	delRxMAT = bsxfun(@plus, delRxMAT, permute(time0wl,[2 3 1]));
	
elseif isempty(beamOriTx) % Plane wave reconstruction
	% Initialize the outputs
	ncols = size(beamDirRx,1);
	delRxMAT = zeros(nrows, nchans, ncols, 'single');
	delTxMAT = zeros(nrows, nxmits, ncols, 'single');
	% Compute the pixel positions
	pixPos = computePixelPositions(beamOriRx, beamDirRx, nrows, spwlRx);
	% Compute the distance to pixel positions for each channel
	for C = 1:ncols
		tmp = bsxfun(@minus, pixPos(:,C,:), elemPos);
		delRxMAT(:,:,C) = sqrt(sum(tmp.^2,3));
	end
	% Compute the transmit delays
	for X = 1:nxmits
		for C = 1:ncols
			delTxMAT(:,X,C) = time0wl(X) + ...
				pixPos(:,C,3) / cos(beamDirTx(X,1)) + ...
				pixPos(:,C,1) * sin(beamDirTx(X,1));
		end
	end
	
elseif isempty(beamDirTx) % Diverging wave reconstruction
	% Initialize the outputs
	ncols = size(beamOriRx,1);
	delRxMAT = zeros(nrows, nchans, ncols, 'single');
	delTxMAT = zeros(nrows, nxmits, ncols, 'single');
	% Compute the pixel positions
	pixPos = computePixelPositions(beamOriRx, beamDirRx, nrows, spwlRx);
	% Compute distance to pixel positions for each channel
	for C = 1:ncols
		tmp = bsxfun(@minus, pixPos(:,C,:), elemPos);
		delRxMAT(:,:,C) = sqrt(sum(tmp.^2,3));
	end
	% Compute the transmit delays
	for X = 1:nxmits
		for C = 1:ncols
			tmp = bsxfun(@minus, pixPos(:,C,:), permute(beamOriRx(X,:),[3 1 2]));
			delTxMAT(:,X,C) = sqrt(sum(tmp.^2,3)) + time0wl(X);
		end
	end
end

% Convert from wavelengths to samples @ Tx sampling freq
delRxMAT = delRxMAT * spwlTx;
delTxMAT = delTxMAT * spwlTx;
delRxMAT = elemPos;%pixPos;
end

% Compute the pixel positions in wavelengths
function pixPos = computePixelPositions(beamOri, beamDir, nrows, spwl)
ncols = size(beamOri,1);
pixPos = zeros(nrows, ncols, 3, 'single');
depthInWL = (1:nrows)' / spwl;
for C = 1:ncols
	pixPos(:,C,1) = depthInWL * sin(beamDir(C,1)) * cos(beamDir(C,2));	% x-coordinate
	pixPos(:,C,2) = depthInWL * sin(beamDir(C,2));						% y-coordinate
	pixPos(:,C,3) = depthInWL * cos(beamDir(C,1)) * cos(beamDir(C,2));	% z-coordinate
end
end

