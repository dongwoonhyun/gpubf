function result = testFocus()
close all
%% Testing parameters
verb = 0; gpuverb = 0;
dataFile = '/data/dhyun/code/rtbf/testCases/points/points_L12-3v_FT.mat';
% Set thresholds for errors
msqthresh = 1e-3; % Error threshold for msq error
maxthresh = 1e-0; % Error threshold for any one value
result = true;

%% Load or generate data set
load(dataFile);
sigFocTx = sigFocTx / max(sigFocTx(:)) * (2^10-1); sigFocTx = single(sigFocTx);
sigSynAp = sigSynAp / max(sigSynAp(:)) * (2^10-1); sigSynAp = single(sigSynAp);
spwlRx = 3.7; wlpsRx = 1/spwlRx;
pixPos = computePixelPositions(beamOriTx, beamDirTx, 1024, spwlRx);
% Create Rx delay and apodization tables
[delRx, delTx] = makeDelayTables(elemPos, pixPos, beamOriTx, beamDirTx, spwlTx, time0wl*0);
apodRx = rand(size(delRx), 'single');
apodTx = rand(size(delTx), 'single');
% Load appropriate data set
for i = 1:size(sigFocTx,3); sigFocTx(:,:,i) = hilbert(sigFocTx(:,:,i)); end;
rfdataFT = sigFocTx;
bbdataFT = sigFocTx;
% Apply demodulation if requested
bbdataFT = bsxfun(@times, bbdataFT, exp(-2j*pi*(0:size(bbdataFT,1)-1)'/spcyTx));

%% Run tests
fprintf('RxDelay | RxApod | TxDelay | TxApod | Baseband |   T_in=>T_out  ||  Result  |  Msq. Err  |  Max. Err  |  CPU time  |  GPU time \n');
fprintf('--------|--------|---------|--------|----------|----------------||----------|------------|------------|------------|-----------\n');
str = '  Yes   |   No   |   No    |   No   |    No    | ';
res = runTest(rfdataFT, delRx, [], [], [], spcyTx, wlpsRx, 0, verb, str);
result = result && res;

str = '  Yes   |   No   |   No    |   No   |    Yes   | ';
res = runTest(bbdataFT, delRx, [], [], [], spcyTx, wlpsRx, 1, verb, str);
result = result && res;

str = '  Yes   |   Yes  |   No    |   No   |    No    | ';
% d = data(:,1:nchans,1:ncols);
res = runTest(rfdataFT, delRx, apodRx, [], [], spcyTx, wlpsRx, 0, verb, str);
result = result && res;

str = '  Yes   |   Yes  |   No    |   No   |    Yes   | ';
% d = data(:,1:nchans,1:ncols);
res = runTest(bbdataFT, delRx, apodRx, [], [], spcyTx, wlpsRx, 1, verb, str);
result = result && res;

%% Load or generate data set
activeElems = sort([1:16:size(elemPos,1) 16:16:size(elemPos,1)]);
sigSynAp = sigSynAp / length(activeElems);
% Create Rx delay and apodization tables
[delRx, delTx] = makeDelayTables(elemPos, pixPos, elemPos(activeElems,:), [], spwlTx, repmat(time0wl,size(elemPos,1),1));
apodRx = rand(size(delRx), 'single');
apodTx = ones(size(delTx), 'single');
% Load appropriate data set
for i = 1:size(sigSynAp,3); sigSynAp(:,:,i) = hilbert(sigSynAp(:,:,i)); end;
sigSynAp = sigSynAp(:,:,activeElems);
rfdataSA = sigSynAp;
bbdataSA = sigSynAp;
% Apply demodulation if requested
bbdataSA = bsxfun(@times, bbdataSA, exp(-2j*pi*(0:size(bbdataSA,1)-1)'/spcyTx));

%% Run tests
str = '  Yes   |   No   |   Yes   |   No   |    No    | ';
res = runTest(rfdataSA, delRx, [], delTx, [], spcyTx, wlpsRx, 0, verb, str);
result = result && res;

str = '  Yes   |   No   |   Yes   |   No   |    Yes   | ';
res = runTest(bbdataSA, delRx, [], delTx, [], spcyTx, wlpsRx, 1, verb, str);
result = result && res;

str = '  Yes   |   Yes  |   Yes   |   Yes  |    No    | ';
res = runTest(rfdataSA, delRx, apodRx, delTx, apodTx, spcyTx, wlpsRx, 0, verb, str);
result = result && res;

str = '  Yes   |   Yes  |   Yes   |   Yes  |    Yes   | ';
res = runTest(bbdataSA, delRx, apodRx, delTx, apodTx, spcyTx, wlpsRx, 1, verb, str);
result = result && res;

end

%% Run the test for a given set of tables
function [result, msqerr, maxerr] = ...
	runTest(data, delRx, apodRx, delTx, apodTx, spcy, wlps, isBaseband, verb, str)
msqthresh = evalin('caller','msqthresh'); % Error threshold for msq error
maxthresh = evalin('caller','maxthresh'); % Error threshold for any one value
result = true;

% MATLAB version
tic
focMAT = testFocus_mat(data, delRx, apodRx, delTx, apodTx, spcy, wlps, isBaseband);
tMAT = toc;
% GPU version
[focGPU1, focGPU2, focGPU3, focGPU4] = ...
	testFocus_mex(data, delRx, apodRx, delTx, apodTx, spcy, wlps, isBaseband);

% Measure error for float2=>float2
fprintf('%sfloat2=>float2 || ', str);
msqerr = mean(abs(focMAT(:)-focGPU1(:)).^2) / max(abs(data(:)));
maxerr = max( abs(focMAT(:)-focGPU1(:))) / max(abs(data(:)));
if msqerr < msqthresh && maxerr < maxthresh
	fprintf('Success! | ');
else
	fprintf('Failure! | ');
	result = false;
end
fprintf(' %8.2E  | ',msqerr)
fprintf(' %8.2E  | ',maxerr)
fprintf(' %8.3f  | ',tMAT*1e3)
fprintf(' %8.3f  \n',tGPU(1)*1e3)

% Measure error for short2=>float2
fprintf('%sshort2=>float2 || ', str);
msqerr = mean(abs(focMAT(:)-focGPU2(:)).^2) / max(abs(data(:)));
maxerr = max( abs(focMAT(:)-focGPU2(:))) / max(abs(data(:)));
if msqerr < msqthresh && maxerr < maxthresh
	fprintf('Success! | ');
else
	fprintf('Failure! | ');
	result = false;
end
fprintf(' %8.2E  | ',msqerr)
fprintf(' %8.2E  | ',maxerr)
fprintf(' %8.3f  | ',tMAT*1e3)
fprintf(' %8.3f  \n',tGPU(2)*1e3)

% Measure error for short2=>half2
fprintf('%sshort2=> half2 || ', str);
msqerr = mean(abs(focMAT(:)-focGPU3(:)).^2) / max(abs(data(:)));
maxerr = max( abs(focMAT(:)-focGPU3(:))) / max(abs(data(:)));
if msqerr < msqthresh && maxerr < maxthresh
	fprintf('Success! | ');
else
	fprintf('Failure! | ');
	result = false;
end
fprintf(' %8.2E  | ',msqerr)
fprintf(' %8.2E  | ',maxerr)
fprintf(' %8.3f  | ',tMAT*1e3)
fprintf(' %8.3f  \n',tGPU(3)*1e3)

% Measure error for short2=>short2
fprintf('%sshort2=>short2 || ', str);
msqerr = mean(abs(focMAT(:)-focGPU4(:)).^2) / max(abs(data(:)));
maxerr = max( abs(focMAT(:)-focGPU4(:))) / max(abs(data(:)));
if msqerr < msqthresh && maxerr < maxthresh
	fprintf('Success! | ');
else
	fprintf('Failure! | ');
	result = false;
end
fprintf(' %8.2E  | ',msqerr)
fprintf(' %8.2E  | ',maxerr)
fprintf(' %8.3f  | ',tMAT*1e3)
fprintf(' %8.3f  \n',tGPU(4)*1e3)



if verb > 1
	disp(['MAT time: ' num2str(tMAT) ' seconds.'])
	disp(['GPU time: ' num2str(tGPU) ' seconds.'])
end

figure
set(gcf,'Position',[-400 100 300 800]);
imagesc(real(focMAT(:,:,17)),[-.1 .1]*(2^12-1))
title('CPU')
drawnow
figure
set(gcf,'Position',[-400 100 300 800]);
imagesc(real(focGPU1(:,:,17)),[-.1 .1]*(2^12-1))
title('GPU')
drawnow
% figure
% imagesc(squeeze(abs(sum(focGPU1,2))))
end
