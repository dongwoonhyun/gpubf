function result = testSLSC()

%% Testing parameters
verb = 0; gpuverb = 0;
dataFile = '/data/dhyun/simulation/lesions_vf10-5/data/focdata/cysts-12dB_1/cysts-12dB_1.mat';
maxlag = 16;
% Set thresholds for errors
msqthresh = 1e-6; % Error threshold for msq error
maxthresh = 1e-2;   % Error threshold for any one value
result = true; % Initial value

%% Load data set
data = loadData(dataFile);

%% Run tests
disp(' ')
disp(' CC Mode  | Maxlag |   T_in=>T_out  ||  Result  |  Msq. Err  |  Max. Err  |  CPU Time  |  GPU Time ');
disp('----------|--------|----------------||----------|------------|------------|------------|-----------');
res = runTest(data, maxlag, 0, verb, gpuverb, sprintf(' Ensemble |   %2d   | ', maxlag));
result = result && res;

res = runTest(data, maxlag, 1, verb, gpuverb, sprintf(' Average  |   %2d   | ', maxlag));
result = result && res;

end

%% Function to load data
function data = loadData(dataFile)
if exist(dataFile,'file')
	disp(['Loading ' dataFile '...'])
	load(dataFile, 'focsig');
	data = permute(focsig, [1 3 2]);
	data = squeeze(sum(reshape(data,size(data,1),2,[],size(data,3)),2));
else
	nrows = 256;
	nchans = 128;
	ncols = 64;
	data = 256*randn(nrows, nchans, ncols, 'single') + ...
		1i*256*randn(nrows, nchans, ncols, 'single');
end

% Normalize data
data = data / max(abs(data(:))) * (2^14-1);
end

%% Run the test for a given set of tables
function [result, msqerr, maxerr] = ...
	runTest(data, maxlag, ccmode, verb, gpuverb, str)
msqthresh = evalin('caller','msqthresh'); % Error threshold for msq error
maxthresh = evalin('caller','maxthresh'); % Error threshold for any one value
result = true;

% MATLAB version
tic
ccMAT = testSLSC_mat(data, maxlag, ccmode);
tMAT = toc;

% GPU version
[ccGPU1, ccGPU2, ccGPU3] = testSLSC_mex(data, maxlag, ccmode, gpuverb);

% Measure error
fprintf('%sfloat2=>float  || ', str);
msqerr = mean(abs(ccMAT(:)-ccGPU1(:)).^2);
maxerr = max( abs(ccMAT(:)-ccGPU1(:)));
if msqerr < msqthresh && maxerr < maxthresh
	fprintf('Success! | ');
else
	fprintf('Failure! | ');
	result = false;
end
fprintf(' %8.2E  | ',msqerr)
fprintf(' %8.2E  | ',maxerr)
fprintf(' %8.3f  | ',tMAT*1e3)
fprintf(' %8.3f  \n',tGPU(1)*1e3)

% Measure error
fprintf('%sshort2=>float  || ', str);
msqerr = mean(abs(ccMAT(:)-ccGPU2(:)).^2);
maxerr = max( abs(ccMAT(:)-ccGPU2(:)));
if msqerr < msqthresh && maxerr < maxthresh
	fprintf('Success! | ');
else
	fprintf('Failure! | ');
	result = false;
end
fprintf(' %8.2E  | ',msqerr)
fprintf(' %8.2E  | ',maxerr)
fprintf(' %8.3f  | ',tMAT*1e3)
fprintf(' %8.3f  \n',tGPU(2)*1e3)

% Measure error
fprintf('%s int2 =>float  || ', str);
msqerr = mean(abs(ccMAT(:)-ccGPU3(:)).^2);
maxerr = max( abs(ccMAT(:)-ccGPU3(:)));
if msqerr < msqthresh && maxerr < maxthresh
	fprintf('Success! | ');
else
	fprintf('Failure! | ');
	result = false;
end
fprintf(' %8.2E  | ',msqerr)
fprintf(' %8.2E  | ',maxerr)
fprintf(' %8.3f  | ',tMAT*1e3)
fprintf(' %8.3f  \n',tGPU(3)*1e3)


if verb > 0
	if msqerr > msqthresh
		disp('Mean square error is too large.')
	end
	if maxerr > maxthresh
		disp('Maximum error is too large.')
	end
end
if verb > 1
	disp(['MAT time: ' num2str(tMAT) ' seconds.'])
	disp(['GPU time: ' num2str(tGPU) ' seconds.'])
end
end

