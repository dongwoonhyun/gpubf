%% MATLAB focusing kernel
function apoTx = makeTxApodTable(elemPos, pixPos, beamOriTx, beamDirTx, apers_0)
if nargin == 5 && length(unique(apers_0)) == 1
	elemPos = elemPos( int32(1:128) + apers_0(1), :);
	apers_0 = 0*apers_0;
end

nelems = size(elemPos,1);
nrows = size(pixPos,1);
ncols = size(pixPos,2);
nxmits = max(size(beamOriTx,1), size(beamDirTx,1));
apoTx = zeros(nrows,nxmits,ncols,'single');

if ~isempty(beamOriTx) && ~isempty(beamDirTx) % Focused transmit
	disp('No transmit apodization is necessary for a 1 to 1 Tx/Rx configuration.')
	apoTx = [];
elseif ~isempty(beamDirTx) % Plane wave transmit
	zpos = pixPos(:,1,3);
	for X = 1:nxmits
		xL = elemPos( 1  + apers_0(X), 1);
		xR = elemPos(128 + apers_0(X), 1);
		
		theta = beamDirTx(X,1);
		xposL = zpos * sin(theta) + xL;
		xposR = zpos * sin(theta) + xR;
		
		tmp = bsxfun(@ge, pixPos(:,:,1), xposL) & bsxfun(@le, pixPos(:,:,1), xposR);
		apoTx(:,X,:) = tmp;
		
	end
	if isequal(unique(apoTx),1)
		apoTx = [];
	end
		
elseif ~isempty(beamOriTx) % Diverging wave transmit
	error('Soon.')
else
	error('Specify the Tx beam origin and/or direction.')
end
