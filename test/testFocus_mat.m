%% MATLAB focusing kernel
function focMAT = testFocus_mat(data, delRx, apodRx, delTx, apodTx, spcyTx, wlpsRx, isBaseband)
	[nrows, nchans, ncols] = size(delRx);
	nxmits = size(delTx,2);
	chanIdx = single(repmat(1:nchans,nrows,1));
	% If doing 1-to-1 reconstruction
	if isempty(delTx)
		focMAT = zeros(size(delRx));
		for col = 1:ncols
			focMAT(:,:,col) = interp2(data(:,:,col), chanIdx, delRx(:,:,col)+1, 'linear', 0);
		end
		if isBaseband
			focMAT = focMAT .* exp(-2j*pi*(2*wlpsRx*repmat((1:nrows)'-1,[1 nchans ncols]) - delRx/spcyTx));
		else
			focMAT = focMAT .* exp(-2j*pi*(2*wlpsRx*repmat((1:nrows)'-1,[1 nchans ncols])                 ));
		end
	% Otherwise, do compounding
	else
		focMAT = zeros(size(delRx));
		for col = 1:ncols
			for xmit = 1:nxmits
				% Get transmit apodization
				if ~isempty(apodTx)
					a = apodTx(:,xmit,col);
				else
					a = ones(nrows,1);
				end
				% Get roundtrip delay
				delRT = bsxfun(@plus, delRx(:,:,col),delTx(:,xmit,col));
				% Interpolate data
				tmp = interp2(data(:,:,xmit), chanIdx, delRT+1, 'linear', 0);
				if isBaseband
					tmp = tmp .* exp(-2j*pi*(2*wlpsRx*repmat((1:nrows)'-1,[1 nchans]) - delRT/spcyTx));
				else
					tmp = tmp .* exp(-2j*pi*(2*wlpsRx*repmat((1:nrows)'-1,[1 nchans])                 ));
				end
				focMAT(:,:,col) = focMAT(:,:,col) + bsxfun(@times, tmp, a);
			end
		end
	end
	
	% Apply receive apodization
	if ~isempty(apodRx)
		focMAT = focMAT .* apodRx;
	end
end
