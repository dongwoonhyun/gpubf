%% MATLAB function to compute CCs
function fdata = testEnsembleFilter_mat(data, filtcoefs)

[nrows, nchans, ncols, ntracks] = size(data);

data = reshape(data, [], ntracks);
fdata = data * filtcoefs;
fdata = reshape(fdata, nrows, nchans, ncols, ntracks);

end
