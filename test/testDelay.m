function result = testDelay()
%% Testing parameters
verb = 0; gpuverb = 0;
nxmits = 25;
nchans = 67;%128;
nrows = 256; % User defined image grid
ncols = 96;
sampsPerCycle = 20;
sampsPerWvln = 2*sampsPerCycle; % x2 for roundtrip
wlps = 1/sampsPerWvln;
% Set thresholds for errors
msqthresh = 1e-5; % Error threshold for msq error
maxthresh = 1e-1;   % Error threshold for any one value
result = true;

%% Load or generate data set
eP   = single(cat(2, linspace(-40, 40, nchans)', zeros(nchans,2)));
bOTx = single(cat(2, linspace(-20, 20, nxmits)', zeros(nxmits,2)));
bDTx = single(cat(2, linspace(-pi/8, pi/8, nxmits)', zeros(nxmits,1)));
spwlTx = sampsPerWvln;
t0   = randn(nxmits, 1, 'single');
bORx = single(cat(2, linspace(-30,30,ncols)', zeros(ncols,2)));
bDRx = single(cat(2, linspace(-pi/16,pi/16,ncols)', zeros(ncols,1)));
spwlRx = 1/4.5;

%% Run tests
disp('      Focusing Type      ||  Result  |  Msq. Err  |  Max. Err ');
disp('-------------------------||----------|------------|-----------');
fprintf('       FocTx 1-to-1      || ');
res = runTest(eP, bOTx, bDTx, spwlTx, t0, [], [], [], nrows, verb, gpuverb);
result = result && res;

fprintf('       FocTx DSx4.5      || ');
res = runTest(eP, bOTx, bDTx, spwlTx, t0, [], [], spwlRx, nrows, verb, gpuverb);
result = result && res;

fprintf('          PlnWv          || ');
res = runTest(eP, [], bDTx, spwlTx, t0, bORx, bDRx, spwlRx, nrows, verb, gpuverb);
result = result && res;

fprintf('          SynAp          || ');
res = runTest(eP, bOTx, [], spwlTx, t0, bORx, bDRx, spwlRx, nrows, verb, gpuverb);
result = result && res;

end

%% Run the test for a given set of tables
function [result, msqerr, maxerr] = runTest(elemPos, ...
	beamOriTx, beamDirTx, spwlTx, time0wl, ...
	beamOriRx, beamDirRx, spwlRx, nrows, verb, gpuverb)

msqthresh = evalin('caller','msqthresh'); % Error threshold for msq error
maxthresh = evalin('caller','maxthresh'); % Error threshold for any one value
result = true;

% Cast EVERYTHING as singles to make life simpler.
elemPos   = single(elemPos);
beamOriTx = single(beamOriTx);
beamDirTx = single(beamDirTx);
spwlTx    = single(spwlTx);
time0wl   = single(time0wl);
beamOriRx = single(beamOriRx);
beamDirRx = single(beamDirRx);
spwlRx    = single(spwlRx);
nrows     = single(nrows);
verb      = single(verb);
gpuverb   = single(gpuverb);

% MATLAB version
tic
[delRxMAT,delTxMAT] = testDelay_mat(elemPos, beamOriTx, beamDirTx, spwlTx, ...
	time0wl, beamOriRx, beamDirRx, spwlRx, nrows);
delMAT = cat(1,delRxMAT(:),delTxMAT(:));
tMAT = toc;

% GPU version
tic
% [delRxGPU,delTxGPU] = testDelay_mex(elemPos, beamOriTx, beamDirTx, spwlTx, ...
% 	time0wl, beamOriRx, beamDirRx, spwlRx, nrows, gpuverb);
% delGPU = cat(1,delRxGPU(:),delTxGPU(:));
delGPU = gpuverb * rand(size(delMAT), 'like', delMAT);
tGPU = toc;

% Measure error
msqerr = mean(abs(delMAT(:)-delGPU(:)).^2);
maxerr = max( abs(delMAT(:)-delGPU(:)));
if msqerr < msqthresh && maxerr < maxthresh
	fprintf('Success! | ');
else
	fprintf('Failure! | ');
	result = false;
end
fprintf(' %8.2E  | ',msqerr)
fprintf(' %8.2E  \n',maxerr)

if verb > 0
	if msqerr > msqthresh
		disp('Mean square error is too large.')
	end
	if maxerr > maxthresh
		disp('Maximum error is too large.')
	end
end
if verb > 1
	disp(['MAT time: ' num2str(tMAT) ' seconds.'])
	disp(['GPU time: ' num2str(tGPU) ' seconds.'])
end
end
