/*
 * SLSC.cuh
 *
 *  Created on: Jan 3, 2015
 *      Author: dhyun
 */

#ifndef SLSC_CUH_
#define SLSC_CUH_

#include "gpuBF.cuh"

namespace gpuBF {

template <typename T_in, typename T_out>
class SLSC: public DataProcessor<T_out> {
private:

	// Input
	T_in *d_dat;
	gpuArrayDesc datDesc;
	
	// SLSC parameters and arrays
	int ccmode; // 0 is ensemble (default), 1 is averaging
	int maxlag;
	T_out *d_ccs;
	T_out *d_mag; // Only used with ensemble method
	gpuArrayDesc ccsDesc;

	// Functions
	void initValuesToZero();
	void initializeArrays();
	void computeCorrCoeffs();
	void computeAvgCorrCoeff();
	void computeEnsCorrCoeff();

public:
	SLSC();
	virtual ~SLSC();
	void initialize(DataProcessor<T_in> *F,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	void initialize(T_in *d_input,
					gpuArrayDesc desc,
					int deviceID,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	void reset();

	void setNewInputDevPtr(T_in *d_input);
	void setMaximumLag(int maximumLag);
	void setCCMode(int ccType);
	void makeSLSC();

};

template <typename T_in, typename T_out>
__global__ void computeCCs(T_in dat,
                           int datSize_w,
                           int maxlag,
                           T_out *ccs,
                           int4 ccSize);
template <typename T_out>
__global__ void sumLags(T_out *ccs,
                        int4 ccSize,
                        T_out *img,
                        int imgsize_w,
                        float normFactor);

template <typename T_in, typename T_out>
__global__ void computeCCNumAndDen(T_in dat,
                                   int datSize_w,
                                   int maxlag,
                                   T_out *num,
                                   T_out *den,
                                   int4 ccSize);
template <typename T_out>
__global__ void computeEnsCC(int maxlag,
                             T_out *num,
                             T_out *den,
                             int4 ccSize,
                             T_out *img,
                             int imgSize_w);


}


#endif /* GPUSLSC_CUH_ */
