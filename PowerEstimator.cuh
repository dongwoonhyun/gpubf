/*
 * PowerEstimator.cuh
 *
 *  Created on: Nov 16, 2015
 *      Author: dhyun
 *
 *  This class applies estimates the power of a signal
 *    by accumulating the squared values.
 */

#ifndef POWERESTIMATOR_CUH_
#define POWERESTIMATOR_CUH_

#include "gpuBF.cuh"

namespace gpuBF {

template <typename T>
class PowerEstimator: public DataProcessor<T> {
private:

	// Input
	T *d_dat;
	gpuArrayDesc datDesc; // Description of data
	// Temporary array
	T *d_tmp; // same size as output

	// Functions
	void initValuesToZero();
	void initializeArrays();

public:
	PowerEstimator();
	virtual ~PowerEstimator();
	void initialize(DataProcessor<T> *F,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	void initialize(T *d_focused,
					gpuArrayDesc desc,
					int deviceID,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	void reset();

	void setInputDevPtr(T *d_input);
	void resetPowerEstimator();
	void accumulateEstimates();
	void applyBoxcarFilter(int halfKernel_z, int halfKernel_x);

};

template <typename T>
__global__ void accumulateEstimates_kernel(T *dat,
                                           int4 datSize,
                                           T *out);
template <typename T>
__global__ void applyBoxcarFilter_kernel(T *tmp,
                                         int4 datSize,
                                         int halfks_z,
										 int halfks_x,
                                         T *out);
template <typename T>
__global__ void copyDataToTempArr_kernel(T *out,
                                         int4 datSize,
                                         T *tmp);
}

#endif /* POWERESTIMATOR_CUH_ */
