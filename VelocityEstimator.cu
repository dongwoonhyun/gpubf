/*
 * VelocityEstimator.cu
 *
 *  Created on: Nov 18, 2015
 *      Author: dhyun
 */

#include "VelocityEstimator.cuh"

namespace gpuBF{

template <typename T_in, typename T_out>
VelocityEstimator<T_in,T_out>::VelocityEstimator() {
	initValuesToZero();
}
template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::initValuesToZero() {
	// Base class members
	this->devID = 0;
	this->stream = 0;
	this->isInit = false;
	this->verb = 0;
	memset(&(this->props),   0, sizeof(this->props));
	memset(&(this->outDesc), 0, sizeof(this->outDesc));
	this->d_out = NULL;

	// VelocityEstimator class members
// #if __CUDA_ARCH__ >= 300
// 	memset(&texDesc, 0, sizeof(texDesc));
// 	memset(&resDesc, 0, sizeof(resDesc));
// #endif
	d_dat = NULL;
	memset(&datDesc, 0, sizeof(datDesc));
	ensSize = 0;
	maxlag = 0;
	scaleFactor = 0;
	halfks_z = 0;
	halfks_x = 0;
	useChanData = false;
	d_num = NULL;
	d_den = NULL;
	memset(&sumDesc, 0, sizeof(sumDesc));
}
template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::reset() {
	if (this->verb > 0)
		printf("Clearing VelocityEstimator object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	CCE(cudaSetDevice(this->devID));
	CCE(cudaFree(d_num));
	CCE(cudaFree(d_den));
	CCE(cudaFree(this->d_out));
	initValuesToZero();
}
template <typename T_in, typename T_out>
VelocityEstimator<T_in,T_out>::~VelocityEstimator() {
	reset();
}
template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::initialize(DataProcessor<T_in> *F,
                                   int ensembleLength,
                                   float velocityScaleFactor,
                                   cudaStream_t cudaStream,
                                   int verbosity) {
	// Pass data to initialize function
	initialize(F->getOutputDevPtr(),
	           F->getOutputDesc(),
	           ensembleLength,
	           velocityScaleFactor,
	           F->getDeviceID(),
	           cudaStream,
	           verbosity);
}
template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::initialize(T_in *d_focused,
                                   gpuArrayDesc desc,
                                   int ensembleLength,
                                   float velocityScaleFactor,
                                   int deviceID,
                                   cudaStream_t cudaStream,
                                   int verbosity) {
	// Device information
	this->devID = deviceID;
	this->stream = cudaStream;
	this->verb = verbosity;
	CCE(cudaGetDeviceProperties(&(this->props), this->devID));
	if (this->verb > 0)
		printf("Initializing VelocityEstimator object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	// Input
	d_dat = d_focused;
	datDesc = desc;
	ensSize = ensembleLength;
	// Initialize arrays
	initializeArrays();

	this->isInit = true;
}
template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::initializeArrays() {
	CCE(cudaSetDevice(this->devID));
	// Intermediate buffers for running sums
	sumDesc = datDesc;
	sumDesc.typeSize = sizeof(int);
	CCE(cudaFree(d_num));
	CCE(cudaFree(d_den));
	makePitchedArray((void **)&d_num, &sumDesc);
	makePitchedArray((void **)&d_den, &sumDesc);
	if (this->verb > 1) printDesc(this->devID, sumDesc, "Vel. Est. Num/Den running sum");
	
// #if __CUDA_ARCH__ >= 300
// 	// Set up textures to use the built-in bilinear interpolation hardware
// 	// Use texture objects (CC >= 3.0)
// 	// Texture description
// 	texDesc.addressMode[0]	 = cudaAddressModeBorder;
// 	texDesc.addressMode[1]	 = cudaAddressModeBorder;
// 	texDesc.filterMode	  	 = cudaFilterModeLinear;
// 	texDesc.readMode 	 	 = cudaReadModeElementType;
// 	texDesc.normalizedCoords = 0;

// 	// Resource description
// 	resDesc.res.pitch2D.width  = datDesc.size.x;
// 	resDesc.res.pitch2D.height = datDesc.size.y*datDesc.size.z;
// 	resDesc.res.pitch2D.devPtr = d_dat;
// 	resDesc.resType 		   = cudaResourceTypePitch2D;
// 	resDesc.res.pitch2D.desc   = cudaCreateChannelDesc<T_in>();
// 	resDesc.res.pitch2D.pitchInBytes = datDesc.pitchInBytes;
// 	// Create texture object
// 	CCE(cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL));
// #endif
	
	// Output array
	this->outDesc = datDesc;
	this->outDesc.size.y = datDesc.size.z;
	this->outDesc.size.z = 1;
	this->outDesc.typeSize = sizeof(T_out);
	CCE(cudaFree(this->d_out));
	makePitchedArray((void **)&(this->d_out), &(this->outDesc));
	if (this->verb > 1) printDesc(this->devID, this->outDesc, "Velocity estimate");
}
template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::setInputDevPtr(T_in *d_input) {
	d_dat = d_input;

// #if __CUDA_ARCH__ >= 300
// 	resDesc.res.pitch2D.devPtr = d_dat;
// 	CCE(cudaSetDevice(devID));
// 	// Destroy former texture object
// 	CCE(cudaDestroyTextureObject(tex));
// 	// Create texture object
// 	CCE(cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL));
// #endif
}

template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::setMaximumLag(int maximumLag) {
	maxlag = maximumLag;
	if (this->verb > 1)
		printf("Set maxlag to %d for VelocityEstimator object on GPU #%d (%s).\n",
		       maxlag, this->devID, this->props.name);
}
template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::setHalfKernel(int halfKernel_z, int halfKernel_x) {
	halfks_z = halfKernel_z;
	halfks_x = halfKernel_x;
	if (this->verb > 1)
		printf("Set halfks to %dx%d (total kernel %dx%d) for VelocityEstimator object on GPU #%d (%s).\n",
		       halfks_z, halfks_x, 2*halfks_z+1, 2*halfks_x+1, this->devID, this->props.name);
}
template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::setScaleFactor(float velocityScaleFactor) {
	scaleFactor = velocityScaleFactor;
	if (this->verb > 1)
		printf("Set scaleFactor to %f for VelocityEstimator object on GPU #%d (%s).\n",
		       scaleFactor, this->devID, this->props.name);	
}
// Function for the ensemble CC method
template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::estimateVelocity_chanKasai() {
	CCE(cudaSetDevice(this->devID));
	dim3 B(32, 8, 1);
	dim3 G((sumDesc.size.x-1)/B.x+1,
	       (sumDesc.size.y-1)/B.y+1,
	       (sumDesc.size.z-1)/B.z+1);
// #if __CUDA_ARCH__ >= 300
// 	computeNumAndDen_chanKasai_kernel<<<G,B,0,this->stream>>>(tex,   datDesc.size.w, ensSize, maxlag, d_num, d_den, sumDesc.size);
// #else
	computeNumAndDen_chanKasai_kernel<<<G,B,0,this->stream>>>(d_dat, datDesc.size.w, ensSize, maxlag, d_num, d_den, sumDesc.size);
// #endif
	B = dim3(256, 1, 1);
	G = dim3((this->outDesc.size.x-1)/B.x+1, (this->outDesc.size.y-1)/B.y+1, 1);
	estimateVelocity_chanKasai_kernel<<<G,B,0,this->stream>>>(d_num,
	                                                          d_den, 
	                                                          sumDesc.size,
	                                                          halfks_z,
	                                                          halfks_x,
	                                                          scaleFactor,
	                                                          this->d_out,
	                                                          this->outDesc.size.w);
}
// Function for the ensemble CC method
template <typename T_in, typename T_out>
void VelocityEstimator<T_in,T_out>::estimateVelocity_Kasai() {
	CCE(cudaSetDevice(this->devID));
	dim3 B(32, 8, 1);
	dim3 G((sumDesc.size.x-1)/B.x+1,
	       (sumDesc.size.y-1)/B.y+1,
	       (sumDesc.size.z-1)/B.z+1);
// #if __CUDA_ARCH__ >= 300
// 	computeNumAndDen_Kasai_kernel<<<G,B,0,this->stream>>>(tex,   datDesc.size.w, ensSize, d_num, d_den, sumDesc.size);
// #else
	computeNumAndDen_Kasai_kernel<<<G,B,0,this->stream>>>(d_dat, datDesc.size.w, ensSize, d_num, d_den, sumDesc.size);
// #endif
	B = dim3(256, 1, 1);
	G = dim3((this->outDesc.size.x-1)/B.x+1, (this->outDesc.size.y-1)/B.y+1, 1);
	estimateVelocity_Kasai_kernel<<<G,B,0,this->stream>>>(d_num,
	                                                      d_den, 
	                                                      sumDesc.size,
	                                                      halfks_z,
	                                                      halfks_x,
	                                                      scaleFactor,
	                                                      this->d_out,
	                                                      this->outDesc.size.w);
}
// Device function to load data
// #if __CUDA_ARCH__ >= 300
// __device__ __inline__ T_in loadData(cudaTextureObject_t t, int x, int y, int z, int sx, int sy) {
// 	return tex2D<T_in>(t, x+0.5f, y + sy*z + 0.5f);
// }
// #else
// #endif

template <typename T_in>
__global__ void computeNumAndDen_Kasai_kernel(T_in *dat,
                                              int datSize_w,
                                              int ensSize,
                                              int *num,
                                              int *den,
                                              int4 sumSize) {
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;
	int oidx = row + sumSize.w*col;
	int stride = datSize_w * sumSize.y * sumSize.z;

	if (row < sumSize.x && col < sumSize.z) {
		// Declare variables needed for cross-correlation computation
		int numsum = 0;
		int densum = 0;
		
		for (int k = 0; k < ensSize-1; k++) {
			// Sum the raw data first (use ints to prevent overflow)
			int2 i0 = make_int2(0, 0);
			int2 i1 = make_int2(0, 0);
			for (int chan = 0; chan < sumSize.y; chan++) {
				float2 tmp0 = loadData2(dat, (k+0)*stride+row+datSize_w*(chan+sumSize.y*col));
				i0.x += tmp0.x;
				i0.y += tmp0.y;
				float2 tmp1 = loadData2(dat, (k+1)*stride+row+datSize_w*(chan+sumSize.y*col));
				i1.x += tmp1.x;
				i1.y += tmp1.y;
			}		
			numsum += i1.x*i0.y - i0.x*i1.y;
			densum += i0.x*i1.x + i0.y*i1.y;
		}
		saveData(num, oidx, numsum);
		saveData(den, oidx, densum);
	}
}

template <typename T_in>
__global__ void computeNumAndDen_chanKasai_kernel(T_in *dat,
                                                  int datSize_w,
                                                  int ensSize,
                                                  int maxlag,
                                                  int *num,
                                                  int *den,
                                                  int4 sumSize) {
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;
	int oidx = row + sumSize.w*(chan + sumSize.y*col);
	int stride = datSize_w * sumSize.y * sumSize.z;

	if (row < sumSize.x && chan < sumSize.y && col < sumSize.z) {
		// Declare variables needed for cross-correlation computation
		int numsum = 0;
		int densum = 0;

		for (int k = 0; k < ensSize-1; k++) {
			// Get signal for current channel
			float2 i0 = loadData2(dat, (k+0)*stride+row*datSize_w*(chan+sumSize.y*col));
			float2 i1 = loadData2(dat, (k+1)*stride+row*datSize_w*(chan+sumSize.y*col));

			// If chan is the last element, job is done
			if (chan < sumSize.y-1) {

				// If near the edge of the aperture, only cross-correlate valid lags
				if (chan + maxlag >= sumSize.y)
					maxlag = sumSize.y - chan - 1;

				// Compute and sum short-lag correlation coefficients
				for (int j = chan; j <= chan + maxlag; j++) {
					float2 j0 = loadData2(dat, (k+0)*stride+row*datSize_w*(j+sumSize.y*col));
					float2 j1 = loadData2(dat, (k+1)*stride+row*datSize_w*(j+sumSize.y*col));
					
					numsum += j1.x*i0.y - i0.x*j1.y + i1.x*j0.y - j0.x*i1.y;
					densum += i0.x*j1.x + i0.y*j1.y + j0.x*i1.x + j0.y*i1.y;
				}
			}
		}
		saveData(num, oidx, numsum);
		saveData(den, oidx, densum);
	}
}
template <typename T_out>
__global__ void estimateVelocity_Kasai_kernel(int *num,
                                              int *den,
                                              int4 sumSize,
                                              int halfks_z,
                                              int halfks_x,
                                              float scaleFactor,
                                              T_out *img,
                                              int imgSize_w) {
	int row = blockIdx.x*blockDim.x + threadIdx.x;
	int col = blockIdx.y*blockDim.y + threadIdx.y;

	int numsum = 0;
	int densum = 0;
	if (row < sumSize.x && col < sumSize.z) {
		// Loop over kernel
		for (int x = col-halfks_x; x <= col+halfks_x; x++) {
			for (int z = row-halfks_z; z <= row+halfks_z; z++) {
				if (z >= 0 && z < sumSize.x && x >= 0 && x < sumSize.z) {
						numsum += num[z + sumSize.w*x];
						densum += den[z + sumSize.w*x];
				}
			}
		}
		saveData(img, row+imgSize_w*col, scaleFactor * atan2f((float)numsum,(float)densum));
	}
}
template <typename T_out>
__global__ void estimateVelocity_chanKasai_kernel(int *num,
                                                  int *den,
                                                  int4 sumSize,
                                                  int halfks_z,
                                                  int halfks_x,
                                                  float scaleFactor,
                                                  T_out *img,
                                                  int imgSize_w) {
	int row = blockIdx.x*blockDim.x + threadIdx.x;
	int col = blockIdx.y*blockDim.y + threadIdx.y;

	int numsum = 0;
	int densum = 0;
	if (row < sumSize.x && col < sumSize.z) {
		// Loop over kernel
		for (int x = col-halfks_x; x <= col+halfks_x; x++) {
			for (int z = row-halfks_z; z <= row+halfks_z; z++) {
				if (z >= 0 && z < sumSize.x && x >= 0 && x < sumSize.z) {
					for (int i = 0; i < sumSize.y-1; i++) {
						numsum += num[z + sumSize.w*(i + sumSize.y*x)];
						densum += den[z + sumSize.w*(i + sumSize.y*x)];
					}
				}
			}
		}
		saveData(img, row+imgSize_w*col, scaleFactor * atan2f((float)numsum,(float)densum));
	}
}

template class VelocityEstimator<float2,float>;
template class VelocityEstimator<half2, float>;
template class VelocityEstimator<short2,float>;
template class VelocityEstimator<int2,  float>;


} // namespace gpuBF


