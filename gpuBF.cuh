/** @file gpuBF.cuh
	@author Dongwoon Hyun (dhyun)
	@date Dec 27, 2014
	@bug GPUBF_TYPE_CONFIG==2 is not working yet.
*/

#ifndef GPUBF_CUH_
#define GPUBF_CUH_

#include <stdio.h>
#include <cuda.h>
#include <typeinfo>
#include "helper_cuda.h"
#include <cuda_fp16.h>

// Define macros for excessively long names
#define CCE checkCudaErrors
#define HtoD cudaMemcpyHostToDevice
#define DtoH cudaMemcpyDeviceToHost
const float PI = 3.1415265f;
const int ERRMSGBUFLEN = 128;

namespace gpuBF {
// #ifndef GPUBF_TYPE_CONFIG
// #define GPUBF_TYPE_CONFIG 1
// #endif
// #if GPUBF_TYPE_CONFIG==0 // Slowest, most precise
// typedef short  VSXType;	 ///< Verasonics output type
// typedef float  RawType;  ///< Raw data type
// typedef float2 RawType2; ///< Raw data type (complex)
// typedef float  FocType;  ///< Focused data type
// typedef float2 FocType2; ///< Focused data type (complex)
// typedef float  ImgType;  ///< Image data type
// typedef float2 ImgType2; ///< Image data type (complex)
// #elif GPUBF_TYPE_CONFIG==1 // Faster, less precise
// typedef short  VSXType;	 ///< Verasonics output type
// typedef short  RawType;  ///< Raw data type
// typedef short2 RawType2; ///< Raw data type (complex)
// typedef float  FocType;  ///< Focused data type
// typedef float2 FocType2; ///< Focused data type (complex)
// typedef float  ImgType;  ///< Image data type
// typedef float2 ImgType2; ///< Image data type (complex)
// #elif GPUBF_TYPE_CONFIG==2 // Fastest, least precise
// typedef short  VSXType;	 ///< Verasonics output type
// typedef short  RawType;  ///< Raw data type
// typedef short2 RawType2; ///< Raw data type (complex)
// typedef half   FocType;  ///< Focused data type
// typedef half2  FocType2; ///< Focused data type (complex)
// typedef float  ImgType;  ///< Image data type
// typedef float2 ImgType2; ///< Image data type (complex)
// #endif

/** @brief Struct to keep track of GPU array dimensions.
	
	Often, multi-dimensional arrays of data are stored on the GPU as
	<i>pitched</i> memory for efficient and aligned access. A \f$m\times n\times
	p\f$ array of data may be stored on the GPU as a \f$m'\times n\times p\f$
	array, where \f$m'\ge m\f$. For convenience, a struct has been defined to
	keep track of information such as the array dimensions and the data type
	size.

	The size parameter is a vector of 4 ints containing \f$(m',m,n,p)\f$.

	The wlps parameter is optional; it can be used to additionally keep track of
	the sampling frequency.

 */
struct gpuArrayDesc {
	int4 size; ///< Dimensions of data
	int typeSize; ///< Size of one element in bytes
	size_t pitchInBytes; ///< Array pitch in bytes
	float wlps; ///< Wavelengths per sample
};
/** @brief Get overall data size in bytes. */
__inline__ int getSizeInBytes(gpuArrayDesc D) { return (int)D.pitchInBytes*D.size.y*D.size.z; }
__inline__ void validateDesc(gpuArrayDesc D, const char *file, int line) {
	if (D.size.x <= 0 || D.size.y <= 0 || D.size.z <= 0 || D.size.w < D.size.x ||
			D.pitchInBytes <= D.size.w || D.pitchInBytes % D.size.w != 0) {
		fprintf(stderr, "%s(%i) : Invalid input size parameters.\n%d x %d x %d (%d bytes, %d elems)",
				file, line, D.size.x, D.size.y, D.size.z, (int)D.pitchInBytes, D.size.w);
		return;
	}
}
__inline__ void printDesc(int devID, gpuArrayDesc D, const char *label) {
	// change from stderr
	printf("GPU [%d]: %s size: %d (%d) x %d x %d\n",
			devID, label, D.size.x, D.size.w, D.size.y, D.size.z);
}
/** @brief Make a pitched GPU array using the provided description. */
__inline__ void makePitchedArray(void **d_arr, gpuArrayDesc *D) {
	if (D->size.x > 0 && D->size.y > 0 && D->size.z > 0) {
		CCE(cudaMallocPitch(d_arr,
							&D->pitchInBytes,
							D->size.x*D->typeSize,
							D->size.y*D->size.z));
		D->size.w = D->pitchInBytes / D->typeSize;
	}
}

inline __host__ __device__ void   operator+=(float2 &a, float  s) { a.x += s;   a.y += s;   }
inline __host__ __device__ void   operator+=(float2 &a, float2 s) { a.x += s.x; a.y += s.y; }
inline __host__ __device__ void   operator*=(float2 &a, float  s) { a.x *= s;   a.y *= s;   }
inline __host__ __device__ float2 operator* (float2  a, float  s) { return make_float2(a.x*s,a.y*s); }

// Load 1-data types from 1-data
__device__ __inline__ float  loadData(float   *dat, int idx) { return dat[idx]; }
__device__ __inline__ float  loadData(half    *dat, int idx) { return __half2float(dat[idx]); }
__device__ __inline__ float  loadData(short   *dat, int idx) { return dat[idx]; }
__device__ __inline__ float  loadData(int     *dat, int idx) { return dat[idx]; }
// Load 2-data types from 1-data
__device__ __inline__ float2 loadData2(float  *dat, int idx) { return make_float2(dat[idx], 0); }
__device__ __inline__ float2 loadData2(half   *dat, int idx) { return make_float2(__half2float(dat[idx]), 0); }
__device__ __inline__ float2 loadData2(short  *dat, int idx) { return make_float2(dat[idx], 0); }
__device__ __inline__ float2 loadData2(int    *dat, int idx) { return make_float2(dat[idx], 0); }
// Load 2-data types from 2-data
__device__ __inline__ float2 loadData2(float2 *dat, int idx) { return dat[idx]; }
__device__ __inline__ float2 loadData2(half2  *dat, int idx) { return __half22float2(dat[idx]); }
__device__ __inline__ float2 loadData2(short2 *dat, int idx) { return make_float2(dat[idx].x,dat[idx].y); }
__device__ __inline__ float2 loadData2(int2   *dat, int idx) { return make_float2(dat[idx].x,dat[idx].y); }
// Save 1-data type from 1-data
__device__ __inline__ void   saveData(float   *dat, int idx, float  d) { dat[idx] = d; }
__device__ __inline__ void   saveData(half    *dat, int idx, float  d) { dat[idx] = __float2half(d); }
__device__ __inline__ void   saveData(short   *dat, int idx, float  d) { dat[idx] = d; }
__device__ __inline__ void   saveData(int     *dat, int idx, float  d) { dat[idx] = d; }
// Save 2-data type from 1-data
__device__ __inline__ void   saveData2(float2 *dat, int idx, float  d) { dat[idx] = make_float2(d, 0.f); }
__device__ __inline__ void   saveData2(half2  *dat, int idx, float  d) { dat[idx] = __float22half2_rn(make_float2(d, 0.f)); }
__device__ __inline__ void   saveData2(short2 *dat, int idx, float  d) { dat[idx] = make_short2(short(d), 0); }
__device__ __inline__ void   saveData2(int2   *dat, int idx, float  d) { dat[idx] = make_int2(int(d), 0); }
// Save 2-data type from 2-data
__device__ __inline__ void   saveData2(float2 *dat, int idx, float2 d) { dat[idx] = d; }
__device__ __inline__ void   saveData2(half2  *dat, int idx, float2 d) { dat[idx] = __float22half2_rn(d); }
__device__ __inline__ void   saveData2(short2 *dat, int idx, float2 d) { dat[idx] = make_short2(short(d.x),short(d.y)); }
__device__ __inline__ void   saveData2(int2   *dat, int idx, float2 d) { dat[idx] = make_int2(int(d.x),int(d.y)); }


/** @brief Abstract base class for processing data on the GPU.
	
	All DataProcessor objects have a general structure that is defined in
	gpuBF.cuh. Each DataProcessor object (or child object) is a module that can
	be daisy-chained with other such modules. Ideally, one DataProcessor object
	can be passed as the input to another DataProcessor object, and the
	initializer code of that second object should know how to parse the output
	of the first. For example, the HilbertTransform object can be fed into the
	Focus object, which can in turn be fed into the Bmode object, and each
	object should be able to invoke the various get* functions of the previous
	object to determine how to allocate its own arrays.
	
	This class is an abstract base class, meaning that a DataProcessor object
	cannot be instantiated on its own. To be usable, child classes must
	explicitly define the virtual functions defined in the DataProcessor base
	class.

 */
template <typename T_out>
class DataProcessor {
protected:

	// Device information and CUDA objects
	int devID; ///<Index of the GPU that the device arrays of this object will reside on
	cudaStream_t stream; ///<The CUDA stream on which all real-time operations will execute
	bool isInit; ///< Boolean to indicate whether arrays have been allocated an object is ready for use
	int verb; ///<Level of verbosity. 0 = no output, 1 = some output, 2 = detailed output
	cudaDeviceProp props; ///<A structure containing information about the device devID

	// Required data pointers and structures
	T_out *d_out; ///<A pointer to the output data of the DataProcessor object
	gpuArrayDesc outDesc;///<Description of the output data array dimensions

	// Require functions to dynamically allocate the needed memory for output
	virtual void initValuesToZero() = 0; ///<A function that initializes all values to zero. Must be redefined by each child class
	virtual void initializeArrays() = 0; ///<A function that dynamically allocates any host/device arrays. Must be redefined by each child class

public:
	// All functions must have a way to change the pointer to the input
	// virtual void setInputDevPtr(void *d_in) = 0;

	// Output functions
	/// @brief Returns the GPU index for this object
	int getDeviceID()		 	 { return devID; }
	/// @brief Returns the CUDA stream for this object
	cudaStream_t getCudaStream() { return stream; }
	/// @brief Returns whether the object has been initialized
	bool isInitialized() 		 { return isInit; }
	/// @brief Returns the level of verbosity
	int getVerbosity()			 { return verb; }
	/// @brief Returns the pointer to the output array d_out
	T_out* getOutputDevPtr() 	 { return d_out; }
	/// @brief Returns the data description of the output array
	gpuArrayDesc getOutputDesc() { return outDesc; }

	// Require a function to free dynamically allocated memory
	/// @brief A function that frees all dynamically allocated arrays owned by the object. Must be redefined by each child class
	virtual void reset() = 0;
};
template class DataProcessor<float2>;
template class DataProcessor<half2>;
template class DataProcessor<short2>;
template class DataProcessor<int2>;
template class DataProcessor<float>;
template class DataProcessor<half>;
template class DataProcessor<short>;
template class DataProcessor<int>;

} // namespace gpuBF

#endif /* GPUBF_CUH_ */


