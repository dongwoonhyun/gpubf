/*
 * AlignNxFc.cu
 *
 *  Created on: Apr 8, 2015
 *      Author: dhyun
 *
 */

#include "AlignNxFc.cuh"

namespace gpuBF {

template <typename T_in, typename T_out>
AlignNxFc<T_in,T_out>::AlignNxFc() {
	initValuesToZero();
}
template <typename T_in, typename T_out>
void AlignNxFc<T_in,T_out>::initValuesToZero() {
	// Base class members
	this->devID = 0;
	this->stream = 0;
	this->isInit = false;
	this->verb = 0;
	memset(&(this->props),   0, sizeof(this->props));
	memset(&(this->outDesc), 0, sizeof(this->outDesc));
	this->d_out = NULL;

	// AlignNxFc members
	memset(&texDesc, 0, sizeof(texDesc));
	memset(&resDesc, 0, sizeof(resDesc));
	memset(&rawDesc, 0, sizeof(rawDesc));
	spcy = 0;
	d_raw = NULL;
}

template <typename T_in, typename T_out>
void AlignNxFc<T_in,T_out>::initialize(DataProcessor<T_in> *R,
                                       int samplesPerCycle,
                                       cudaStream_t cudaStream,
                                       int verbosity) {
	initialize(R->getOutputDevPtr(),
	           R->getOutputDesc(),
	           samplesPerCycle,
	           R->getDeviceID(),
	           cudaStream,
	           verbosity);
}
template <typename T_in, typename T_out>
void AlignNxFc<T_in,T_out>::initialize(T_in *d_complexRF,
                                       gpuArrayDesc inputDesc,
                                       int samplesPerCycle,
                                       int deviceID,
                                       cudaStream_t cudaStream,
                                       int verbosity) {
	// Base class members
	this->devID = deviceID;
	this->stream = cudaStream;
	this->verb = verbosity;
	CCE(cudaGetDeviceProperties(&(this->props), this->devID));
	if (this->verb > 0)
		printf("Initializing AlignNxFc object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	this->outDesc = inputDesc;

	// AlignNxFc members
	d_raw = d_complexRF;
	rawDesc = inputDesc;
	spcy = samplesPerCycle;

	// Initialize arrays
	initializeArrays();
	this->isInit = true;
}

template <typename T_in, typename T_out>
void AlignNxFc<T_in,T_out>::initializeArrays() {
	CCE(cudaSetDevice(this->devID));
	// Initialize texture object on input data
	initTextureObject(d_raw);
	// Initialize output array
	makePitchedArray((void **)&(this->d_out), &(this->outDesc));
	if (this->verb > 1) printDesc(this->devID, this->outDesc, "Complex IQ data");
	CCE(cudaMemset((this->d_out), 0, getSizeInBytes(this->outDesc)));
}
// void AlignNxFc::setInputDevPtr(void *d_in) {
// 	d_raw = d_in;
// 	CCE(cudaDestroyTextureObject(tex));
// 	initTextureObject(d_in);
// }

template <typename T_in>
inline void setTextureObjectModes(T_in *d_ptr, cudaTextureDesc &texDesc) {
	texDesc.readMode = cudaReadModeNormalizedFloat;
}
template <>
inline void setTextureObjectModes(float2 *d_ptr, cudaTextureDesc &texDesc) {
	texDesc.readMode = cudaReadModeElementType;
}

template <typename T_in, typename T_out>
void AlignNxFc<T_in,T_out>::initTextureObject(T_in *d_ptr) {
	// Set up textures to use the built-in bilinear interpolation hardware
	// Use texture objects (CC >= 3.0)
	// Texture description
	texDesc.addressMode[0]	 = cudaAddressModeBorder;
	texDesc.addressMode[1]	 = cudaAddressModeBorder;
	texDesc.filterMode	  	 = cudaFilterModeLinear;
	texDesc.normalizedCoords = 0;
	setTextureObjectModes(d_ptr, texDesc); // Set type dependent modes

	// Resource description
	resDesc.resType 		   = cudaResourceTypePitch2D;
	resDesc.res.pitch2D.width  = rawDesc.size.x;
	resDesc.res.pitch2D.height = rawDesc.size.y*rawDesc.size.z;
	resDesc.res.pitch2D.desc   = cudaCreateChannelDesc<T_in>();
	resDesc.res.pitch2D.devPtr = d_ptr;
	resDesc.res.pitch2D.pitchInBytes = rawDesc.pitchInBytes;
	// Create texture object
	CCE(cudaSetDevice(this->devID));
	CCE(cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL));
}

template <typename T_in, typename T_out>
void AlignNxFc<T_in,T_out>::reset() {
	if (this->verb > 0)
		printf("Clearing AlignNxFc object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	CCE(cudaSetDevice(this->devID));
	CCE(cudaFree(this->d_out));
	CCE(cudaDestroyTextureObject(tex));
	initValuesToZero();
	this->isInit = false;
}

template <typename T_in, typename T_out>
AlignNxFc<T_in,T_out>::~AlignNxFc() {
	reset();
}

template <typename T_in, typename T_out>
void AlignNxFc<T_in,T_out>::alignBasebandData() {
	dim3 B(256,1,1);
	dim3 G((rawDesc.size.w-1)/B.x+1, rawDesc.size.y, rawDesc.size.z);
	if (spcy == 4) {
		negateEveryOtherKernel<<<G,B,0,this->stream>>>(d_raw, rawDesc.size, d_raw);
		alignQuadratureKernel<T_out,4><<<G,B,0,this->stream>>>(tex, rawDesc.size, this->d_out);
	}
	else if (spcy == 2)
		alignQuadratureKernel<T_out,2><<<G,B,0,this->stream>>>(tex, rawDesc.size, this->d_out);
	else if (spcy == 1)
		alignQuadratureKernel<T_out,1><<<G,B,0,this->stream>>>(tex, rawDesc.size, this->d_out);
	else
		printf("Samples per cycle must equal 1, 2, or 4. spcy == %d.\n", spcy);
}

// Kernels
template <typename T_in>
__global__ void negateEveryOtherKernel(T_in *orig, int4 size, T_in *tmp) {
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	int tidy = threadIdx.y + blockIdx.y*blockDim.y;
	int tidz = threadIdx.z + blockIdx.z*blockDim.z;
	int idx = tidx + size.w*(tidy + size.y*tidz);
	int sign = (1 - 2*(tidx & 0x1));
	// Check bounds
	if (tidx < size.w && tidy < size.y && tidz < size.z) {
		float2 data = loadData2(orig, idx);
		data.x *= sign;
		data.y *= sign;
		saveData2(tmp, idx, data);
	}
}
template <typename T>
__device__ __inline__ float normFactor(T dat) { return 32767.f; }
template <>
__device__ __inline__ float normFactor(float2 dat) { return 1.f; }

template <typename T_out, int spw>
__global__ void alignQuadratureKernel(cudaTextureObject_t tex, int4 size, T_out *data) {
	int tidx = threadIdx.x + blockIdx.x*blockDim.x;
	int tidy = threadIdx.y + blockIdx.y*blockDim.y;
	int tidz = threadIdx.z + blockIdx.z*blockDim.z;
	int idx = tidx + size.w*(tidy + size.y*tidz);

	// Check bounds
	if (tidx < size.x && tidy < size.y && tidz < size.z) {
		// Obtain the aligned
		float2 unal = tex2D<float2>(tex,tidx+0.5f,tidy+size.y*tidz+0.5f);
		// Align the data using linear interpolation
		float2 algn = tex2D<float2>(tex,tidx+0.5f-(spw/8.0f),tidy+size.y*tidz+0.5f);
		// Only align the quadrature component
		float2 d;
		d.x = unal.x * normFactor(unal.x);
		d.y = algn.y * normFactor(unal.x) * -1.f;
		// The complex conjugate is taken simply to match the quadrature
		// component with the standard output of the Hilbert transform.
		saveData2(data, idx, d);
	}
}
template class AlignNxFc<short2,float2>;
template class AlignNxFc<short2,short2>;
template class AlignNxFc<short2, half2>;
template class AlignNxFc<short2,  int2>;
template class AlignNxFc<float2,float2>;
template class AlignNxFc<float2,short2>;
template class AlignNxFc<float2, half2>;
template class AlignNxFc<float2,  int2>;

} // namespace gpuBF
