/*
 * PowerEstimator.cu
 *
 *  Created on: Jan 3, 2015
 *      Author: dhyun
 */

#include "PowerEstimator.cuh"

namespace gpuBF{

template <typename T>
PowerEstimator<T>::PowerEstimator() {
	initValuesToZero();
}
template <typename T>
void PowerEstimator<T>::initValuesToZero() {
	// Base class members
	this->devID = 0;
	this->stream = 0;
	this->isInit = false;
	this->verb = 0;
	memset(&(this->props),   0, sizeof(this->props));
	memset(&(this->outDesc), 0, sizeof(this->outDesc));
	this->d_out = NULL;

	// PowerEstimator members
	d_dat = NULL;
	memset(&datDesc, 0, sizeof(datDesc));
	d_tmp = NULL;
}
template <typename T>
void PowerEstimator<T>::reset() {
	if (this->verb > 0)
		printf("Clearing PowerEstimator object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	CCE(cudaSetDevice(this->devID));
	CCE(cudaFree(this->d_out));
	CCE(cudaFree(d_tmp));
	initValuesToZero();
}
template <typename T>
PowerEstimator<T>::~PowerEstimator() {
	reset();
}
template <typename T>
void PowerEstimator<T>::initialize(DataProcessor<T> *F,
                                   cudaStream_t cudaStream,
                                   int verbosity) {
	// Pass data to initialize function
	initialize(F->getOutputDevPtr(),
	           F->getOutputDesc(),
	           F->getDeviceID(),
	           cudaStream,
	           verbosity);
}
template <typename T>
void PowerEstimator<T>::initialize(T *d_focused,
                                   gpuArrayDesc desc,
                                   int deviceID,
                                   cudaStream_t cudaStream,
                                   int verbosity) {
	// Base class members
	this->devID = deviceID;
	this->stream = cudaStream;
	this->verb = verbosity;
	CCE(cudaGetDeviceProperties(&(this->props), this->devID));
	if (this->verb > 0)
		printf("Initializing PowerEstimator object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	// PowerEstimator members
	d_dat = d_focused;
	datDesc = desc;
	// Initialize arrays
	initializeArrays();

	this->isInit = true;
}
template <typename T>
void PowerEstimator<T>::initializeArrays() {
	CCE(cudaSetDevice(this->devID));
	this->outDesc = datDesc;
	this->outDesc.typeSize = sizeof(T);
	CCE(cudaFree(d_tmp));
	makePitchedArray((void **)&d_tmp, &(this->outDesc));
	CCE(cudaFree(this->d_out));
	makePitchedArray((void **)&(this->d_out), &(this->outDesc));
	if (this->verb > 1) printDesc(this->devID, this->outDesc, "Power estimate");
	resetPowerEstimator();
}
template <typename T>
void PowerEstimator<T>::setInputDevPtr(T *d_input) {
	d_dat = d_input;
}
template <typename T>
void PowerEstimator<T>::resetPowerEstimator() {
	// Initialize values to zero
	CCE(cudaSetDevice(this->devID));
	CCE(cudaMemset(      d_tmp, 0, getSizeInBytes(this->outDesc)));
	CCE(cudaMemset(this->d_out, 0, getSizeInBytes(this->outDesc)));
}
template <typename T>
void PowerEstimator<T>::accumulateEstimates() {
	CCE(cudaSetDevice(this->devID));
	// This function applies the filter to both the real and imaginary
	// components by treating each complex number as two real numbers
	dim3 B(256, 1, 1);
	dim3 G((this->outDesc.size.x-1)/B.x+1,
    	   (this->outDesc.size.y-1)/B.y+1,
    	   (this->outDesc.size.z-1)/B.z+1);
	accumulateEstimates_kernel<<<G,B,0,this->stream>>>(d_dat,
	                                                   datDesc.size,
	                                                   this->d_out);
}

template <typename T>
void PowerEstimator<T>::applyBoxcarFilter(int halfKernel_z, int halfKernel_x) {
	CCE(cudaSetDevice(this->devID));
	// This function applies the filter to both the real and imaginary
	// components by treating each complex number as two real numbers
	dim3 B(256, 1, 1);
	dim3 G((this->outDesc.size.x-1)/B.x+1,
    	   (this->outDesc.size.y-1)/B.y+1,
    	   (this->outDesc.size.z-1)/B.z+1);
	copyDataToTempArr_kernel<<<G,B,0,this->stream>>>(this->d_out,
	                                                 this->outDesc.size,
	                                                 d_tmp);
	applyBoxcarFilter_kernel<<<G,B,0,this->stream>>>(d_tmp,
	                                                 this->outDesc.size,
	                                                 halfKernel_z,
	                                                 halfKernel_x,
	                                                 this->d_out);
}

template <typename T>
__global__ void accumulateEstimates_kernel(T *dat,
                                           int4 datSize,
                                           T *out) {
	// Information about the thread
	int row = blockIdx.x*blockDim.x + threadIdx.x;
	int col = blockIdx.y*blockDim.y + threadIdx.y;
	int idx = row + datSize.w*col;

	// Limit computation to valid indices
	if (row < datSize.x && col < datSize.y) {
		// Accumulate power estimates
		float tmp = loadData(dat, idx);
		saveData(out, idx, loadData(out, idx) + tmp*tmp);
	}
}

template <typename T>
__global__ void copyDataToTempArr_kernel(T *out,
                                         int4 datSize,
                                         T *tmp) {
	// Information about the thread
	int row = blockIdx.x*blockDim.x + threadIdx.x;
	int col = blockIdx.y*blockDim.y + threadIdx.y;
	int idx = row + datSize.w*col;

	// Limit computation to valid indices
	if (row < datSize.x && col < datSize.y) {
		saveData(tmp, idx, loadData(out, idx));
	}
}
template <typename T>
__global__ void applyBoxcarFilter_kernel(T *tmp,
                                         int4 datSize,
                                         int halfks_z,
										 int halfks_x,
                                         T *out) {
	// Information about the thread
	int row = blockIdx.x*blockDim.x + threadIdx.x;
	int col = blockIdx.y*blockDim.y + threadIdx.y;

	// Limit computation to valid indices
	if (row < datSize.x && col < datSize.y) {
		float sum = 0.0f;
		// Loop over kernel
		for (int x = col-halfks_x; x <= col+halfks_x; x++) {
			for (int z = row-halfks_z; z <= row+halfks_z; z++) {
				if (z >= 0 && z < datSize.x && x >= 0 && x < datSize.y) {
					// Accumulate power estimates
					sum += loadData(tmp, z+datSize.w*x);
				}
			}
		}
		saveData(out, row+datSize.w*col, sum);
	}
}


template class PowerEstimator<float>;
template class PowerEstimator<short>;
template class PowerEstimator<half>;

} // namespace gpuBF


