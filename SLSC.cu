/*
 * SLSC.cu
 *
 *  Created on: Jan 3, 2015
 *      Author: Dongwoon Hyun
 */

#include "SLSC.cuh"

namespace gpuBF{

template <typename T_in, typename T_out>
SLSC<T_in,T_out>::SLSC() {
	initValuesToZero();
}
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::initValuesToZero() {
	// Base class members
	this->devID = 0;
	this->stream = 0;
	this->isInit = false;
	this->verb = 0;
	memset(&(this->props),   0, sizeof(this->props));
	memset(&(this->outDesc), 0, sizeof(this->outDesc));
	this->d_out = NULL;

	// SLSC members
	d_dat = NULL;
	memset(&datDesc, 0, sizeof(datDesc));
	ccmode = 0;
	maxlag = 0;
	d_ccs = NULL;
	d_mag = NULL;
	memset(&ccsDesc, 0, sizeof(ccsDesc));
}
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::reset() {
	if (this->verb > 0)
		printf("Clearing SLSC object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	CCE(cudaSetDevice(this->devID));
	CCE(cudaFree(d_ccs));
	CCE(cudaFree(d_mag));
	CCE(cudaFree(this->d_out));
	initValuesToZero();
}
template <typename T_in, typename T_out>
SLSC<T_in,T_out>::~SLSC() {
	reset();
}
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::initialize(DataProcessor<T_in> *F,
					  cudaStream_t cudaStream,
					  int verbosity) {
	initialize(F->getOutputDevPtr(),
			   F->getOutputDesc(),
			   F->getDeviceID(),
			   cudaStream,
			   verbosity);
}
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::initialize(T_in *d_focused,
					  gpuArrayDesc desc,
					  int deviceID,
					  cudaStream_t cudaStream,
					  int verbosity) {
	// Base class members
	this->devID = deviceID;
	this->stream = cudaStream;
	this->verb = verbosity;
	CCE(cudaGetDeviceProperties(&(this->props), this->devID));
	if (this->verb > 0)
		printf("Initializing SLSC object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	this->outDesc = desc;

	// SLSC members
	d_dat = d_focused;
	datDesc = desc;
	// Initialize arrays
	initializeArrays();
	this->isInit = true;
}
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::initializeArrays() {
	CCE(cudaSetDevice(this->devID));
	
	// Create device array to hold computed cc's
	ccsDesc = datDesc;
	ccsDesc.typeSize = sizeof(T_out);
	CCE(cudaFree(d_ccs));
	makePitchedArray((void **)&d_ccs, &ccsDesc);
	if (this->verb > 1) printDesc(this->devID, ccsDesc, "CC data");
	if (ccmode == 0) {
		CCE(cudaFree(d_mag));
		makePitchedArray((void **)&d_mag, &ccsDesc);
		if (this->verb > 1) printDesc(this->devID, ccsDesc, "CC (denominator) data");
	}

	// Set default maxlag value to ~1/4 of the aperture
	maxlag = int(ccsDesc.size.y/4);

	// Create output image
	this->outDesc = datDesc;
	this->outDesc.size.y = datDesc.size.z;
	this->outDesc.size.z = 1;
	this->outDesc.typeSize = sizeof(T_out);
	CCE(cudaFree(this->d_out));
	makePitchedArray((void **)&(this->d_out), &(this->outDesc));
	if (this->verb > 1) printDesc(this->devID, this->outDesc, "SLSC data");

}
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::setMaximumLag(int maximumLag) {
	maxlag = maximumLag;
	if (this->verb > 1)
		printf("Set maxlag to %d for SLSC object on GPU #%d (%s).\n",
		       maxlag, this->devID, this->props.name);
}
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::setCCMode(int ccType) {
	ccmode = ccType;
	if (this->verb > 1)
		printf("Set ccmode to %d for SLSC object on GPU #%d (%s).\n",
		       ccmode, this->devID, this->props.name);
}
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::setNewInputDevPtr(T_in *d_input) {
	d_dat = d_input;
}

// Functions for the averaging CC method
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::computeCorrCoeffs() {
	CCE(cudaSetDevice(this->devID));
	dim3 B(256, 1, 1);
	dim3 G((ccsDesc.size.x-1)/B.x+1, (ccsDesc.size.y-2)/B.y+1, (ccsDesc.size.z-1)/B.z+1);
	computeCCs<<<G,B,0,this->stream>>>(d_dat, datDesc.size.w, maxlag, d_ccs, ccsDesc.size);
}
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::computeAvgCorrCoeff() {
	// There are MN - M*(M+1)/2 lags for a maxlag of M and N elements.
	// normFactor = 1 / M*(N-(M+1)/2)
	float normFactor = 1.0f / (maxlag * (ccsDesc.size.y-(maxlag+1)/2.f));
	CCE(cudaSetDevice(this->devID));
	dim3 B(256, 1, 1);
	dim3 G((this->outDesc.size.x-1)/B.x+1, (this->outDesc.size.y-1)/B.y+1, 1);
	sumLags<<<G,B,0,this->stream>>>(d_ccs, ccsDesc.size, this->d_out, this->outDesc.size.w, normFactor);
}

// Function for the ensemble CC method
template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::computeEnsCorrCoeff() {
	CCE(cudaSetDevice(this->devID));
	dim3 B(32, 8, 1);
	dim3 G((ccsDesc.size.x-1)/B.x+1, (ccsDesc.size.y-1)/B.y+1, (ccsDesc.size.z-1)/B.z+1);
	computeCCNumAndDen<<<G,B,0,this->stream>>>(d_dat, datDesc.size.w, maxlag, d_ccs, d_mag, ccsDesc.size);
	B = dim3(256, 1, 1);
	G = dim3((this->outDesc.size.x-1)/B.x+1, (this->outDesc.size.y-1)/B.y+1, 1);
	computeEnsCC<<<G,B,0,this->stream>>>(maxlag, d_ccs, d_mag, ccsDesc.size, this->d_out, this->outDesc.size.w);
}

template <typename T_in, typename T_out>
void SLSC<T_in,T_out>::makeSLSC() {
	if (ccmode == 0) {
		computeEnsCorrCoeff();
	}
	else if (ccmode == 1) {
		computeCorrCoeffs();
		computeAvgCorrCoeff();
	}
}

// __device__ __inline__ float2 loadData2(T_in *t, int x, int y, int z, int sx, int sy) {
// 	// return (x < sx && y < sy) ? t[x + sx*(y + sy*z)] : make_float2(0.0f);
// #if GPUBF_TYPE_CONFIG==0 || GPUBF_TYPE_CONFIG==1
// 	return t[x + sx*(y + sy*z)];
// #elif GPUBF_TYPE_CONFIG==2
// 	return __half22float2(t[x + sx*(y + sy*z)]);
// #endif
// }

// Averaging method kernels
template <typename T_in, typename T_out>
__global__ void computeCCs(T_in dat,
                           int datSize_w,
                           int maxlag,
                           T_out *ccs,
                           int4 ccSize) {
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;

	if (row < ccSize.x && chan < ccSize.y-1 && col < ccSize.z) {

		// If near the edge of the aperture, only cross-correlate valid lags
		if (chan + maxlag >= ccSize.y)
			maxlag = ccSize.y - chan - 1;

		// Declare variables needed for cross-correlation computation
		float sum = 0.0f;

		// Get signal for channel
		float2 sig0 = loadData2(dat, row+datSize_w*(chan+ccSize.y*col));
		sig0 *= rsqrtf(sig0.x*sig0.x + sig0.y*sig0.y); // normalize

		// Compute and sum short-lag correlation coefficients
		for (int i = chan+1; i <= chan + maxlag; i++) {
			float2 sigm = loadData2(dat, row+datSize_w*(i+ccSize.y*col));
			sigm *= rsqrtf(sigm.x*sigm.x + sigm.y*sigm.y); // normalize
			sum += sig0.x*sigm.x + sig0.y*sigm.y;
		}
		saveData(ccs, row + ccSize.w*(chan + ccSize.y*col), sum);
	}
}
template <typename T_out>
__global__ void sumLags(T_out *ccs,
                        int4 ccSize,
                        T_out *img,
                        int imgSize_w,
                        float normFactor) {
	int row = blockIdx.x*blockDim.x + threadIdx.x;
	int col = blockIdx.y*blockDim.y + threadIdx.y;

	if (row < ccSize.x && col < ccSize.z) {
		float sum = 0.0f;
		for (int i = 0; i < ccSize.y-1; i++)
			sum += loadData(ccs, row + ccSize.w*(i + ccSize.y*col));

		// There are MN - M*(M+1)/2 lags for a maxlag of M and N elements.
		// normFactor = 1 / (MN - M*(M+1)/2)
		saveData(img, row + imgSize_w*col, sum * normFactor);
	}
}

// Ensemble method kernels
template <typename T_in, typename T_out>
__global__ void computeCCNumAndDen(T_in dat,
                                   int datSize_w,
                                   int maxlag,
                                   T_out *num,
                                   T_out *den,
                                   int4 ccSize) {
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;

	if (row < ccSize.x && chan < ccSize.y && col < ccSize.z) {

		// Get signal for current channel and store its magnitude
		float2 sig0 = loadData2(dat, row+datSize_w*(chan+ccSize.y*col));
		saveData(den, row + ccSize.w*(chan + ccSize.y*col), sig0.x*sig0.x + sig0.y*sig0.y);

		// If chan is the last element, job is done
		if (chan < ccSize.y-1) {

			// If near the edge of the aperture, only cross-correlate valid lags
			if (chan + maxlag >= ccSize.y)
				maxlag = ccSize.y - chan - 1;

			// Declare variables needed for cross-correlation computation
			float sum = 0.0f;

			// Compute and sum short-lag correlation coefficients
			for (int i = chan+1; i <= chan + maxlag; i++) {
				float2 sigm = loadData2(dat, row+datSize_w*(i+ccSize.y*col));
				sum += sig0.x*sigm.x + sig0.y*sigm.y;
			}
			saveData(num, row + ccSize.w*(chan + ccSize.y*col), sum);
		}
	}
}

template <typename T_out>
__global__ void computeEnsCC(int maxlag, T_out *num, T_out *den, int4 ccSize, T_out *img, int imgSize_w) {
	int row = blockIdx.x*blockDim.x + threadIdx.x;
	int col = blockIdx.y*blockDim.y + threadIdx.y;

	if (row < ccSize.x && col < ccSize.z) {
		// Compute running sum of numerator
		float numsum = 0.0f;
		for (int i = 0; i < ccSize.y-1; i++)
			numsum += loadData(num, row + ccSize.w*(i + ccSize.y*col));

		// Compute sum of denominator, and account for repeats
		float densum_i = 0.0f;
		float densum_j = 0.0f;
		for (int i = 0; i < maxlag; i++) {
			densum_i +=   i    * loadData(den, row + ccSize.w*(i + ccSize.y*col));
			densum_j += maxlag * loadData(den, row + ccSize.w*(i + ccSize.y*col));
		}
		for (int i = maxlag; i < ccSize.y-maxlag; i++) {
			densum_i += maxlag * loadData(den, row + ccSize.w*(i + ccSize.y*col));
			densum_j += maxlag * loadData(den, row + ccSize.w*(i + ccSize.y*col));
		}
		for (int i = ccSize.y-maxlag; i < ccSize.y; i++) {
			densum_i +=     maxlag     * loadData(den, row + ccSize.w*(i + ccSize.y*col));
			densum_j += (ccSize.y-i-1) * loadData(den, row + ccSize.w*(i + ccSize.y*col));
		}

		// Compute correlation coefficient
		saveData(img, row + imgSize_w*col, numsum * rsqrtf(densum_i*densum_j));
	}
}

template class SLSC<float2,float>;
template class SLSC<half2, float>;
template class SLSC<short2,float>;
template class SLSC<int2,  float>;


} // namespace gpuBF


