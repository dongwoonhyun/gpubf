/** @file HilbertTransform.cuh
	@author Dongwoon Hyun (dhyun)
	@date Dec 27, 2014

	This class returns the complex quadrature component of a real signal.	
 */

#ifndef HILBERTTRANSFORM_CUH_
#define HILBERTTRANSFORM_CUH_

#include "gpuBF.cuh"
#include <cufft.h>

namespace gpuBF {
/** @brief Class to apply Hilbert Transform

	This class takes a real signal (stored as an interleaved complex array
	with zeros for the imaginary component) and computes the Hilbert Transform
	using the cuFFT API. The transform is accomplished via a forward FFT,
	eliminating the negative frequencies and doubling the positive
	frequencies, followed by a final inverse FFT.
*/
template <typename T_in, typename T_out>
class HilbertTransform: public DataProcessor<T_out> {
private:

	// CUDA objects
	cufftHandle fftplan;

	// Input data
	T_in *d_raw;
	gpuArrayDesc rawDesc;

	// Intermediate array
	float2 *d_fft;
	gpuArrayDesc fftDesc;

	// Initialization functions
	void initValuesToZero();
	void initializeArrays();

public:
	// Constructor and destructor
	HilbertTransform();
	virtual ~HilbertTransform();

	// Initializer
	void initialize(DataProcessor<T_in> *R,
	                cudaStream_t cudaStream=0,
	                int verbosity=1);
	void initialize(T_in *d_complexRF,
	                gpuArrayDesc inputDesc,
	                int deviceID=0,
	                cudaStream_t cudaStream=0,
	                int verbosity=1);
	// Hilbert transform of raw RF data
	void applyHilbertTransform();

	// Free dynamically allocated memory
	void reset();
};

// Kernels
__global__ void hilbertWeightKernelOdd(float2 *data, int4 size);
__global__ void hilbertWeightKernelEven(float2 *data, int4 size);
template <typename T_in>
__global__ void convertToTypeFloat2(T_in *raw,
                                    int4 rawSize,
                                    float2 *fft,
                                    int fftSize_w);
template <typename T_out>
__global__ void convertFromTypeFloat2(float2 *fft,
                                      int4 fftSize,
                                      T_out *out,
                                      int outSize_w);
}

#endif /* HILBERTTRANSFORM_CUH_ */
