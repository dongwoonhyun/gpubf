/*
 * Bmode.cu
 *
 *  Created on: Dec 30, 2014
 *      Author: dhyun
 */

#include "Bmode.cuh"

namespace gpuBF {

template <typename T_in, typename T_out>
Bmode<T_in,T_out>::Bmode() {
	initValuesToZero();
}
template <typename T_in, typename T_out>
void Bmode<T_in,T_out>::initValuesToZero() {
	// Base class members
	this->devID = 0;
	this->stream = 0;
	this->isInit = false;
	this->verb = 0;
	memset(&(this->props),   0, sizeof(this->props));
	memset(&(this->outDesc), 0, sizeof(this->outDesc));
	this->d_out = NULL;

	// Bmode class members
	memset(&focDesc,0,sizeof(focDesc));
	d_foc = NULL;
}
template <typename T_in, typename T_out>
void Bmode<T_in,T_out>::reset() {
	if (this->verb > 0)
		printf("Clearing Bmode object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	CCE(cudaSetDevice(this->devID));
	CCE(cudaFree(this->d_out));
	initValuesToZero();
}
template <typename T_in, typename T_out>
Bmode<T_in,T_out>::~Bmode() {
	reset();
}
template <typename T_in, typename T_out>
void Bmode<T_in,T_out>::initialize(DataProcessor<T_in> *F,
                                   cudaStream_t cudaStream,
                                   int verbosity) {
	initialize(F->getOutputDevPtr(),
	           F->getOutputDesc(),
	           F->getDeviceID(),
	           cudaStream,
	           verbosity);
}
template <typename T_in, typename T_out>
void Bmode<T_in,T_out>::initialize(T_in *d_focused,
                                   gpuArrayDesc desc,
                                   int deviceID,
                                   cudaStream_t cudaStream,
                                   int verbosity) {
	// Device information
	this->devID = deviceID;
	this->stream = cudaStream;
	this->verb = verbosity;
	CCE(cudaGetDeviceProperties(&(this->props), (this->devID)));
	if (this->verb > 0)
		printf("Initializing Bmode object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	// Input
	d_foc = d_focused;
	focDesc = desc;
	// Initialize arrays
	initializeArrays();
	this->isInit = true;
}
template <typename T_in, typename T_out>
void Bmode<T_in,T_out>::initializeArrays() {
	CCE(cudaSetDevice(this->devID));
	this->outDesc = focDesc;
	this->outDesc.size.y = focDesc.size.z;
	this->outDesc.size.z = 1;
	this->outDesc.typeSize = sizeof(T_out);
	CCE(cudaFree(this->d_out));
	makePitchedArray((void **)&(this->d_out), &(this->outDesc));
	if (this->verb > 1) printDesc(this->devID, this->outDesc, "B-mode data");
}
template <typename T_in, typename T_out>
void Bmode<T_in,T_out>::makeBmode(bool applyCompression) {
	dim3 B(256, 1, 1);
	dim3 G((this->outDesc.size.x-1)/B.x+1, this->outDesc.size.y, 1);
	if (!applyCompression)
		sumDetectChannelData<<<G,B,0,this->stream>>>(d_foc, focDesc.size, this->d_out, this->outDesc.size.w);
	else
		sumDetectCompressChannelData<<<G,B,0,this->stream>>>(d_foc, focDesc.size, this->d_out, this->outDesc.size.w);
}

template <typename T_in, typename T_out>
void Bmode<T_in,T_out>::setInputDevPtr(T_in *d_input) {
	d_foc = d_input;
}

template <typename T_in, typename T_out>
__global__ void sumDetectChannelData(T_in *foc,
                                     int4 focSize,
                                     T_out *out,
                                     int outSize_w) {
	int row = threadIdx.x + blockIdx.x*blockDim.x;
	int col = threadIdx.y + blockIdx.y*blockDim.y;
	if (row < focSize.x && col < focSize.z) {
		// Sum IQ data
		float2 sum = make_float2(0.0f, 0.0f);
		for (int chan = 0; chan < focSize.y; chan++) {
			float2 dat;
			dat = loadData2(foc, row + focSize.w*(chan + focSize.y*col));
			sum.x += dat.x;
			sum.y += dat.y;
		}
		// Store output
		saveData(out, row + outSize_w*col, sqrtf(sum.x*sum.x + sum.y*sum.y));
	}
}
template <typename T_in, typename T_out>
__global__ void sumDetectCompressChannelData(T_in *foc,
                                             int4 focSize,
                                             T_out *out,
                                             int outSize_w) {
	int row = threadIdx.x + blockIdx.x*blockDim.x;
	int col = threadIdx.y + blockIdx.y*blockDim.y;
	if (row < focSize.x && col < focSize.z) {
		// Sum IQ data
		float2 sum = make_float2(0.0f, 0.0f);
		for (int chan = 0; chan < focSize.y; chan++) {
			float2 dat;
			dat = loadData2(foc, row + focSize.w*(chan + focSize.y*col));
			sum.x += dat.x;
			sum.y += dat.y;
		}
		// Store output
		saveData(out, row + outSize_w*col, -20.f*log10(rsqrtf(sum.x*sum.x + sum.y*sum.y)));
	}
}
template class Bmode<float2,float>;
template class Bmode<half2, float>;
template class Bmode<short2,float>;
template class Bmode<int2,  float>;

} // namespace gpuBF
