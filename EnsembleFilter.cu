/*
 * EnsembleFilter.cu
 *
 *  Created on: Jan 3, 2015
 *      Author: dhyun
 */

#include "EnsembleFilter.cuh"

namespace gpuBF{

template <typename T>
EnsembleFilter<T>::EnsembleFilter() {
	initValuesToZero();
}
template <typename T>
void EnsembleFilter<T>::initValuesToZero() {
	// Base class members
	this->devID = 0;
	this->stream = 0;
	this->isInit = false;
	this->verb = 0;
	memset(&(this->props),   0, sizeof(this->props));
	memset(&(this->outDesc), 0, sizeof(this->outDesc));
	this->d_out = NULL;

	// EnsembleFilter members
	d_dat = NULL;
	memset(&datDesc, 0, sizeof(datDesc));
	d_filt = NULL;
}
template <typename T>
void EnsembleFilter<T>::reset() {
	if (this->verb > 0)
		printf("Clearing EnsembleFilter object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	CCE(cudaSetDevice(this->devID));
	CCE(cudaFree(d_filt));
	initValuesToZero();
}
template <typename T>
EnsembleFilter<T>::~EnsembleFilter() {
	reset();
}
template <typename T>
void EnsembleFilter<T>::initialize(DataProcessor<T> *F,
                                   int ensembleLength,
                                   float *h_filterCoefficients,
                                   cudaStream_t cudaStream,
                                   int verbosity) {
	// Pass data to initialize function
	initialize(F->getOutputDevPtr(),
	           F->getOutputDesc(),
	           ensembleLength,
	           h_filterCoefficients,
	           F->getDeviceID(),
	           cudaStream,
	           verbosity);
}
template <typename T>
void EnsembleFilter<T>::initialize(T *d_focused,
                                   gpuArrayDesc desc,
                                   int ensembleLength,
                                   float *h_filterCoefficients,
                                   int deviceID,
                                   cudaStream_t cudaStream,
                                   int verbosity) {
	// Base class members
	this->devID = deviceID;
	this->stream = cudaStream;
	this->verb = verbosity;
	CCE(cudaGetDeviceProperties(&(this->props), this->devID));
	if (this->verb > 0)
		printf("Initializing EnsembleFilter object on GPU #%d (%s).\n",
		       this->devID, this->props.name);
	this->outDesc = desc;

	// Input
	d_dat = d_focused;
	datDesc = desc;
	ensSize = ensembleLength;
	if (ensSize > MAXENSEMBLELENGTH)
		fprintf(stderr, "%s(%i) : %s\n",
				        __FILE__, __LINE__,
				        "The ensemble size is too large (max 16).");
	// Initialize arrays
	initializeArrays();
	// Copy filter coefficients from host to device
	CCE(cudaSetDevice(this->devID));
	CCE(cudaFree(d_filt));
	CCE(cudaMalloc((void **)&d_filt, sizeof(float)*ensSize*ensSize));
	CCE(cudaMemcpy(d_filt,
	    		   h_filterCoefficients,
	    		   sizeof(float)*ensSize*ensSize,
	    		   HtoD));

	this->isInit = true;
}
template <typename T>
void EnsembleFilter<T>::initializeArrays() {
	CCE(cudaSetDevice(this->devID));
	// CUBLAS requires a separate output array for the multiplication.
	this->d_out = d_dat;
	this->outDesc = datDesc;
}
template <typename T>
void EnsembleFilter<T>::applyEnsembleFilter() {
	CCE(cudaSetDevice(this->devID));
	// This function applies the filter to both the real and imaginary components
	dim3 B(256, 1, 1);
	dim3 G((this->outDesc.size.x-1)/B.x+1,
    	   (this->outDesc.size.y-1)/B.y+1,
    	   (this->outDesc.size.z-1)/B.z+1);
	int smemSize = sizeof(float)*ensSize*ensSize;
	applyEnsembleFilter_kernel<<<G,B,smemSize,this->stream>>>(d_dat,
    	                                                datDesc.size,
        	                                            ensSize,
            	                                        d_filt);
}

template <typename T>
__global__ void applyEnsembleFilter_kernel(T *dat,
                                           int4 datSize,
                                           int ensSize,
                                           float *filt) {
	extern __shared__ float fc[]; // Use shared memory for filter coefficients
	// Information about the thread
	int row  = blockIdx.x*blockDim.x + threadIdx.x;
	int chan = blockIdx.y*blockDim.y + threadIdx.y;
	int col  = blockIdx.z*blockDim.z + threadIdx.z;
	int tidx = threadIdx.x + blockDim.x*(threadIdx.y + blockDim.y*threadIdx.z);
	int didx = row + datSize.w*(chan + datSize.y*col);
	int stride = datSize.w*datSize.y*datSize.z;

	// Load filter coefficients into shared memory
	if (tidx < ensSize*ensSize)
		fc[tidx] = filt[tidx];
	__syncthreads();

	// Limit computation to valid indices
	if (row < datSize.x && chan < datSize.y && col < datSize.z) {
		// Load all data tracks into registers
		float2 d[MAXENSEMBLELENGTH];
		for (int i = 0; i < ensSize; i++) {
			d[i] = loadData2(dat, didx+stride*i);
		}
		// Compute each component of output one by one
		for (int j = 0; j < ensSize; j++) {
			float2 sum;
			sum.x = 0.0f; sum.y = 0.0f;
			for (int i = 0; i < ensSize; i++) {
				sum.x += d[i].x * fc[i + ensSize*j];
				sum.y += d[i].y * fc[i + ensSize*j];
			}
			// Store output
			saveData2(dat, didx+stride*j, sum);
		}
	}
}

template class EnsembleFilter<float2>;
template class EnsembleFilter<short2>;
template class EnsembleFilter<half2>;

} // namespace gpuBF


