/*
 * EnsembleFilter.cuh
 *
 *  Created on: Nov 16, 2015
 *      Author: dhyun
 *
 *  This class applies a high-pass filter in place across a slow time ensemble.
 *  (The output array is in the same memory location as the input array.)
 */

#ifndef ENSEMBLEFILTER_CUH_
#define ENSEMBLEFILTER_CUH_

#define MAXENSEMBLELENGTH 16

#include "gpuBF.cuh"

namespace gpuBF {

template <typename T>
class EnsembleFilter: public DataProcessor<T> {
private:

	// Input
	T *d_dat;
	gpuArrayDesc datDesc; // Description of data
	int ensSize; // Size of the ensemble (i.e., # of tracks/collinear events)
	float *d_filt;

	// Functions
	void initValuesToZero();
	void initializeArrays();

public:
	EnsembleFilter();
	virtual ~EnsembleFilter();
	void initialize(DataProcessor<T> *F,
	                int ensembleLength,
	                float *h_filterCoefficients,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	void initialize(T *d_focused,
					gpuArrayDesc desc,
	                int ensembleLength,
	                float *h_filterCoefficients,
					int deviceID,
					cudaStream_t cudaStream=0,
					int verbosity=1);
	void reset();

	void applyEnsembleFilter();

};

template <typename T>
__global__ void applyEnsembleFilter_kernel(T *dat,
                                           int4 datSize,
                                           int ensSize,
                                           float *filt);

}

#endif /* ENSEMBLEFILTER_CUH_ */
